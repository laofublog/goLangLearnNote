package main

import (
	"fmt"
	//"github.com/PuerkitoBio/goquery"

	//"github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	//fmt.Println(strings.Join(getCookie(), ""))
	itemId := "6833749648750936580"
	url := "https://www.ixigua.com/" + itemId + "?wid_try=1"

	//url := "https://www.ixigua.com/6833749648750936580?id=6811073326656520718&logTag=zowZzZwHL7e_JEDAyfyVd&wid_try=1"
	client := &http.Client{
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	req.Header.Add("Pragma", "no-cache")
	//req.Header.Add("Cookie", "ixigua-a-s=3; Hm_lpvt_db8ae92f7b33b6596893cdf8c004a1a2=1608997396; Hm_lvt_db8ae92f7b33b6596893cdf8c004a1a2=1608557632,1608817059,1608911824,1608977037; _ga=GA1.2.1493495660.1608995647; _gid=GA1.2.1140355746.1608995647; MONITOR_WEB_ID=9945fa46-716b-4b5e-b90a-ae5d03377dd1; ttwid=1%7Cs4rjdisNZXiUdoXMZOwhcyZHUge9v-OGhK9tvmb8omQ%7C1608995019%7Cd319321091e251346d2335903d979bd6718ef98cfedb2cdafa57a0ee1075ae0d; xiguavideopcwebid=6910583642030163460; xiguavideopcwebid.sig=9jY9I-6d4AdPDnjmwBNTzhSSkh4")
	req.Header.Add("Referer", "https://www.ixigua.com")
	req.Header.Add("Cache-Control", "no-cache")
	req.Header.Add("Host", "www.ixigua.com")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15")
	//req.Header.Add("Accept-Language", "zh-cn")
	//req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("Cookie", strings.Join(getCookie(), ""))

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	fmt.Println(body)
	//document,err:= goquery.NewDocument(string(body))
	//fmt.Println(document.Find(".projection-series-list"))
	//getDetail("6811073326656520718")
}

func getCookie() []string {
	url := "https://www.ixigua.com/?wid_try=1"
	method := "GET"

	client := &http.Client{
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	//req.Header.Add("Pragma", "no-cache")
	req.Header.Add("Referer", "https://www.ixigua.com/")
	//req.Header.Add("Cache-Control", "no-cache")
	req.Header.Add("Host", "www.ixigua.com")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15")
	//req.Header.Add("Accept-Language", "zh-cn")
	//req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	req.Header.Add("Connection", "keep-alive")
	//req.Header.Add("Cookie", "xiguavideopcwebid=6910588810619880971; xiguavideopcwebid.sig=KpDn2qZaePHFsHPmA_VDMEQ_J10; ixigua-a-s=0")

	res, err := client.Do(req)
	defer res.Body.Close()
	//body, err := ioutil.ReadAll(res.Body)
	//fmt.Println(string(body))
	var arr []string
	for _, c := range res.Cookies() {
		arr = append(arr, c.String())
	}
	fmt.Println(arr)
	return arr
}
//
//func getDetail(id string) string {
//	url := "https://www.ixigua.com/api/public/videov2/brief/details?group_id=" + id
//	client := &http.Client{
//	}
//	req, err := http.NewRequest(http.MethodGet, url, nil)
//
//	if err != nil {
//		fmt.Println(err)
//	}
//	res, err := client.Do(req)
//	defer res.Body.Close()
//	body, err := ioutil.ReadAll(res.Body)
//
//	fmt.Println(string(body))
//	return ""
//}
