package main

import (
	"bufio"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	)

var wg sync.WaitGroup //定义一个同步等待的组
var pool chan struct{}

func main() {

	fmt.Println("请选择下载方式（默认 1）：")
	fmt.Println("1. 单个视频")
	fmt.Println("2. 合集下载")
	fmt.Print("请输入：")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	defer  scanner.Scan()
	defer  fmt.Print("下载完成，按任意键退出....")


	downTypeStr := scanner.Text()
	if downTypeStr == "" {
		downTypeStr = "1"
	}
	downType, err := strconv.Atoi(downTypeStr)
	if err != nil {
		downType = 1
	}
	//fmt.Println(downType)
	fmt.Print("请输入 URL：")
	scanner.Scan()
	mainUrl := scanner.Text()
	if downType == 1 {
		downloadSingal(mainUrl)
		return
	}
	itemId := strings.Split(strings.Split(mainUrl, "?")[0], "/")[3]
	downSerias(itemId, 5)

	//str := "i6498481090188018189, i6498142859596661261, i6498142855297499661, i6497745722757087757, i6497375574242099725, i6496996870110315022, i6496685466086015502, i6496290145145192974, i6495989777664311822, i6495910356475445774, i6495881119173444109, i6495881119173444109"
	//titleUrl := "https://www.ixigua.com/"
	//strArr := strings.Split(str, ",")
	//pool = make(chan struct{}, 5)
	//for i := 0; i < len(strArr); i++ {
	//	go func(j int) {
	//		downloadSingal(titleUrl + strings.Trim(strArr[j], " ")[1:] + "?logTag=SSgROEEjOxBEpLGv2PBhY")
	//
	//	}(i)
	//
	//}
	//<-pool
	//<-pool
	//<-pool
	//<-pool
	//<-pool
	//<-pool

}

func downloadSingal(mainUrl string) {

	fmt.Println("单个下载：" + mainUrl)
	paras := strings.Split(mainUrl, "?")[1]
	parasArr := strings.Split(strings.Split(paras, "&")[0], "=")
	var idValue string
	if parasArr[0] == "id" {
		idValue = parasArr[1]
		fmt.Println("解析合集Url中的一集,id为：", idValue)
	} else {
		idValue = strings.Split(strings.Split(mainUrl, "?")[0], "/")[3]
		fmt.Println("解析url,id为：", idValue)
	}
	downUrl, title := getDetail(idValue)
	download(downUrl, title)
	return
}

func downSerias(itemId string, count int) {
	serias := getSerias(itemId)
	//限制线程目的
	pool = make(chan struct{}, count)
	for _, s := range serias {
		m := s.(map[string]interface{})
		url, title := getDetail(m["item_id"].(string))
		wg.Add(1)
		pool <- struct{}{}
		go func() {
			defer wg.Done()
			defer func() {
				<-pool
			}()
			download(url, title)
		}()
	}
	wg.Wait()
	fmt.Println("下载完成！！")
}

func getDetail(id string) (string, string) {
	url := "https://www.ixigua.com/api/public/videov2/brief/details?group_id=" + id
	client := &http.Client{
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	res, dErr := client.Do(req)
	if dErr != nil {
		panic(dErr)
	}
	defer res.Body.Close()
	body, rEerr := ioutil.ReadAll(res.Body)
	if rEerr != nil {
		panic(rEerr)
	}
	m := make(map[string]interface{})
	jsonErr := json.Unmarshal(body, &m)
	if jsonErr != nil {
		panic(jsonErr)
	}
	dataMap := m["data"].(map[string]interface{})
	//fmt.Println(dataMap)
	title := dataMap["title"]
	//fmt.Println(title)
	videoMap := dataMap["videoResource"].(map[string]interface{})
	resourceMap := videoMap["normal"].(map[string]interface{})
	vMap := resourceMap["video_list"].(map[string]interface{})
	vEntity := vMap["video_"+strconv.Itoa(len(vMap))].(map[string]interface{})
	mainBase64Url := vEntity["main_url"].(string)
	vUrl, bErr := base64.StdEncoding.DecodeString(mainBase64Url)
	if bErr != nil {
		panic(bErr)
	}
	//vMap:=videoArr[0]
	mainUrl := string(vUrl)
	fmt.Printf("title:%s,url:%s\n", title, mainUrl)
	return mainUrl, title.(string)
}

func getSerias(id string) []interface{} {

	url := "https://www.ixigua.com/api/videov2/pseries_more_v2?pSeriesId=" + id + "&rank=0&tailCount=200"
	method := "GET"

	client := &http.Client{
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("authority", "www.ixigua.com")
	req.Header.Add("accept", "application/json, text/plain, */*")
	req.Header.Add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
	req.Header.Add("referer", "https://www.ixigua.com/6833749648750936580")
	req.Header.Add("accept-language", "zh-CN,zh;q=0.9,en;q=0.8")

	res, dErr := client.Do(req)
	if dErr != nil {
		panic(dErr)
	}
	defer res.Body.Close()
	body, rErr := ioutil.ReadAll(res.Body)
	if rErr != nil {
		panic(rErr)
	}
	m := make(map[string]interface{})
	jsonErr := json.Unmarshal(body, &m)
	if jsonErr != nil {
		panic(jsonErr)
	}
	dataMap := m["data"].([]interface{})
	return dataMap
}

func download(url string, title string) {
	title = strings.Replace(title, "/", "", -1)
	fmt.Println(title, "开始下载...")
	client := &http.Client{
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		panic(err)
	}
	res, resErr := client.Do(req)
	if resErr != nil {
		panic(err)
	}
	defer res.Body.Close()
	f, cErr := os.Create(title + ".mp4")
	if cErr != nil {
		panic(cErr)
	}
	io.Copy(f, res.Body)
	//wg.Done()
}
