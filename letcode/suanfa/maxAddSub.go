package main

import (
	"fmt"
)

func main() {
	arr := []int{-2, 1, -3, 4, -1, 2, 1, -5, 4}
	sub := maxAddSub(&arr, 0, len(arr)-1)
	fmt.Println(sub)
}

func maxAddSubDp(arr []int) int {
	res := arr[0]
	for i := 1; i < len(arr); i++ {
		arr[i] += MaxValue(arr[i-1], 0)
		res = MaxValue(res, arr[i])
	}
	return res
}

func maxAddSub(p *[]int, from, to int) int {
	arr := *p
	if from == to {
		return (arr)[0]
	}
	mid := (from + to) / 2
	frontValue := maxAddSub(p, from, mid)
	endValue := maxAddSub(p, mid+1, to)

	now := arr[mid]
	left := 0
	for i := mid - 1; i >= 0; i-- {
		now += arr[i]
		if now > left {
			left = now
		}
	}
	now = arr[mid+1]
	right := arr[mid+1]
	for i := mid + 2; i < len(arr); i++ {
		now += arr[i]
		if now > right {
			right = now
		}
	}
	totalValue := right + left
	m := MaxValue(frontValue, endValue)
	return MaxValue(totalValue, m)
}

func MaxValue(v1, v2 int) int {
	if v1 > v2 {
		return v1
	}
	return v2
}
