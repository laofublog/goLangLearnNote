package main

import "fmt"

func main() {
	num1:=[]int{1,3}
	num2:=[]int{2,4}
	result := findMedianSortedArrays(num1, num2)
	fmt.Println(result)

}
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	l1 := len(nums1)
	l2 := len(nums2)
	mid := (l1 + l2) / 2
	i, j := 0, 0
	pre, next := 0, 0
	for i+j != mid+1 {
		pre = next
		if i < l1 && (j >= l2 || nums1[i] < nums2[j]) {
			next = nums1[i]
			i++
		} else {
			next = nums2[j]
			j++
		}
	}
	if (l1+l2)&1 == 0 {
		return float64(pre+next) / 2
	}

	return float64(next)
}
