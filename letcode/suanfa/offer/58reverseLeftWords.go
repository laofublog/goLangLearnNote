package main

import "fmt"

func main() {
	str := "abcdefg"
	words := reverseLeftWords(str, 2)
	fmt.Println(words)
}

func reverseLeftWords(s string, n int) string {
	tArr := s[0:n]
	fArr := s[n :len(s)]

	return fArr + tArr
}
