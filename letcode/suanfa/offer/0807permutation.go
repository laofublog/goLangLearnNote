package main

import "fmt"

func main() {
	str := permutationdp("abcd")
	fmt.Println(len(str))
	fmt.Println(str)
}

func permutation(S string) []string {
	var arr []string
	if len(S) == 1 {
		arr = append(arr, S)
		return arr
	}
	for i := 0; i < len(S); i++ {
		s := S[0:i] + S[i+1:len(S)]
		tmpArr := permutation(s)
		for j := 0; j < len(tmpArr); j++ {
			tmpArr[j] = string(S[i]) + tmpArr[j]
		}

		arr = append(arr, tmpArr...)
	}
	return arr

}

func permutationdp(S string) []string {
	n := len(S)
 	var result []string
	if n <= 1 {
		return []string{S}
	}
	result = append(result, string(S[0]))
	current := 1
	for len(result) > 0 && len(S) > current {
		var tmp []string
		for i := 0; i < len(result); i++ {
			for j := 0; j < len(result[i])+1; j++ {
				s := result[i][0:j] + string(S[current]) + result[i][j:len(result[i])]
				tmp = append(tmp, s)
			}
		}
		current++
		result = tmp
	}
	return result
}
