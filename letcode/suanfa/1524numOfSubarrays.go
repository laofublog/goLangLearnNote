package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7}
	subarrays := numOfSubarrays(arr)
	fmt.Println(subarrays)
}
func numOfSubarrays(arr []int) int {
	odd, even := 0, 1
	sum := 0
	count := 0
	for i := 0; i < len(arr); i++ {
		sum += arr[i]
		if sum&1 == 0 {
			count += odd
			even++
		} else {
			count += even
			odd++
		}
	}
	return count
}
