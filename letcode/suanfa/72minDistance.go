package main

import "fmt"

func main() {
	distance := minDistance("horse", "ros")
	fmt.Println(distance)
}

func minDistance(word1 string, word2 string) int {
	m := len(word1)
	n := len(word2)
	dp := make([][]int, m+1)
	for i := 0; i <= m; i++ {
		dp[i] = make([]int, n+1)
	}
	dp[0][0] = 0
	// dp[i][0]表示编辑为空串的代价，即为将所有字符删除的代价
	for i := 0; i < m+1; i++ {
		dp[i][0] = i
	}
	// dp[0][i]表示将空串编辑为str2[:i]的代价，即插入字符的代价

	for i := 0; i < n+1; i++ {
		dp[0][i] = i
	}
	for i := 1; i < m+1; i++ {
		for j := 1; j < n+1; j++ {
			if word1[i-1] == word2[j-1] {
				dp[i][j] = dp[i-1][j-1]
			} else {
				dp[i][j] = minValueThree(dp[i-1][j-1], dp[i-1][j], dp[i][j-1]) + 1
			}
			print(dp)
		}
	}
	return dp[m][n]
}

func minValueThree(v1, v2, v3 int) int {
	value := minValue(v1, v2)
	return minValue(value, v3)
}

func minValue(v1, v2 int) int {
	if v1 < v2 {
		return v1
	}
	return v2
}

func print(arr [][]int) {
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[0]); j++ {
			fmt.Print(arr[i][j], "	")
		}
		fmt.Println("")

	}
	fmt.Println("====================")
}
