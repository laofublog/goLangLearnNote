package main

import (
	"fmt"
)

func main() {
	palindrome := isPalindrome(12231)
	fmt.Println(palindrome)
}
func isPalindrome(x int) bool {
	if x<0{
		return false
	}
	if x<10 {
		return false
	}
	revertedNumber := 0
	for x > revertedNumber {
		revertedNumber = revertedNumber * 10 + x % 10
		x /= 10
	}

	return x == revertedNumber || x == revertedNumber / 10

}