package main

import "fmt"

func main() {
	str := "ababa"
	palindrome := longestPalindrome(str)
	fmt.Println(palindrome)
}

func longestPalindrome1(s string) string {
	if len(s) == 1 {
		return s
	}
	n := len(s)
	ans := ""
	dp := make([][]int, n)
	for i := 0; i < n; i++ {
		dp[i] = make([]int, n)
	}
	for i := 0; i < n; i++ {
		dp[i][i] = 1
		if i < n-1 && s[i] == s[i+1] {
			dp[i][i+1] = 1
		}
		printArr(dp)
	}
	for i := 0; i < n; i++ {
		for j := i + 2; j < n; j++ {
			if s[i] == s[j] {
				dp[i][j] = dp[i+1][j-1]
			}
			printArr(dp)
		}
	}

	printArr(dp)
	return ans
}
func longestPalindrome(s string) string {
	n := len(s)
	ans := ""
	dp := make([][]int, n)
	for i := 0; i < n; i++ {
		dp[i] = make([]int, n)
	}

	for l := 0; l < n; l++ {
		for i := 0; i+l < n; i++ {
			j := i + l
			if l == 0 {
				dp[i][j] = 1
				printArr(dp)

			} else if l == 1 {
				if s[i] == s[j] {
					dp[i][j] = 1
				}
				printArr(dp)

			} else {
				if s[i] == s[j] {
					dp[i][j] = dp[i+1][j-1]
				}
			}
			if dp[i][j] > 0 && l+1 > len(ans) {
				ans = s[i : i+l+1]
			}
			printArr(dp)
		}
	}
	return ans
}

func printArr(arr [][]int) {
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[0]); j++ {
			fmt.Print(arr[i][j], "	")
		}
		fmt.Println()
	}
	fmt.Println("=================")
}

//-----------------------------Manacher(马拉车) 算法   O(N)  ---------------------------------------
// https://segmentfault.com/a/1190000008484167
func longestPalindrome2(s string) string {
	strBuf := make([]byte, 1)
	result := make([]byte, 0)
	strBuf[0] = '#'
	for i := 0; i < len(s); i++ {
		strBuf = append(strBuf, s[i], '#')
	}
	var res, start int
	// 获取要求字符串的长度和位置
	for i := 0; i < len(strBuf); i++ {
		var count = 1
		for j := 1; i-j >= 0 && i+j < len(strBuf); j++ {
			if strBuf[i-j] == strBuf[i+j] {
				if count < j*2+1 {
					count = j*2 + 1
				}
			} else {
				break
			}
		}
		if res < count {
			res = count
			start = i - count/2
		}
	}
	// 获取要求的字符串
	for i := start; i < start+res; i++ {
		if strBuf[i] != '#' {
			result = append(result, strBuf[i])
		}
	}
	return string(result)

}
