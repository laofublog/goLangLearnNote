package utils

type Queue []interface{}

func (q *Queue) Push(value interface{}) {
	*q = append(*q, value)
}

func (q *Queue) Pop() interface{} {
	p := (*q)[0]
	*q = (*q)[1:]
	return p
}
