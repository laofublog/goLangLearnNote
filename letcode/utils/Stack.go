package utils

type Stack []interface{}

func (sk *Stack) Push(val interface{}) {
	*sk = append(*sk, val)
}

func (sk *Stack) Len() int {
	return len(*sk)
}
func (sk *Stack) Top() interface{} {
	sklen := len(*sk) - 1
	return (*sk)[sklen]
}
func (sk *Stack) Pop() interface{} {
	sklen := len(*sk) - 1
	if sklen < 0 {
		return nil
	}
	i := (*sk)[sklen]
	*sk = (*sk)[:sklen]
	return i
}

//func main() {
//	stack := &Stack{}
//	stack.push(1)
//	stack.push(2)
//	stack.push(3)
//	stack.push(4)
//	stack.push(5)
//	stack.push(6)
//	for len(*stack) > 0 {
//		fmt.Println(stack.pop())
//	}
//}
//
