package main

import "fmt"

func main() {
	arr1, arr2 := []int {1,3},[]int{2}
fmt.Println(findMedianSortedArrays(arr1, arr2))
}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	l1 := len(nums1)
	l2 := len(nums2)
	s := l1 + l2
	h := s / 2
	if l1 > h {
		if s%2 == 0 {
			res := (float64(nums1[h-1]) + float64(nums1[h])) / 2
			return res
		}
		res := float64(nums1[h])
		return res
	}

	if s%2 == 0 {
		res := float64(nums2[h-l1-1]+nums2[h-l1]) / 2
		return res
	}
	res := float64(nums2[h-l1+1])
	return res
}
