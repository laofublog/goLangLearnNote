package main

import "fmt"

func main() {
	haystack := ""
	needle := "a"
	str := strStr(haystack, needle)
	fmt.Println(str)
}
func strStr(haystack string, needle string) int {
	hayLen := len(haystack)
	needLen := len(needle)
	if needLen == 0 {
		return 0
	}
	phayLen := 0
	pneedLen := 0
	for phayLen < hayLen && pneedLen < needLen {
		if haystack[phayLen] == needle[pneedLen] {
			phayLen++
			pneedLen++
			if pneedLen == needLen {
				return phayLen - needLen
			}
		} else {
			phayLen = phayLen - pneedLen + 1
			pneedLen = 0
		}
	}

	return -1
}
