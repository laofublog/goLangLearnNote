package main

import (
	"fmt"
	"math"
)

func main() {
	i := reverse(1534236469)
	fmt.Println(i)
}

func reverse(x int) int {
	value := 0
	for x != 0 {
		pop := x % 10
		if value > math.MaxInt32/10 || (value == math.MaxInt32/10 && pop > 7) {
			return 0
		}
		if value < math.MinInt32/10 || (value == math.MinInt32/10 && pop < -8) {
			return 0
		}
		value = value*10 + pop
		x = x / 10
	}
	return value
}
