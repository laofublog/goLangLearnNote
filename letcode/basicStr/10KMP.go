package main

import (
	"fmt"
)

func main() {
	str1 := ""
	str2 := ""
	match := kmpMatch(str1, str2)
	fmt.Println(str1[match-len(str2) : match])
	fmt.Println(match)

}

func kmpMatch(str1, str2 string) int {
	if len(str2)==0{
		return 0
	}
	next := getNext(str2)
	p1 := 0
	p2 := 0
	hasMatchCount := 0
	for p1 < len(str1) {
		if str1[p1] == str2[p2] {
			p2++
			p1++
			hasMatchCount++
		} else {
			if p2 > 0 {
				p2 = next[p2-1]
			} else {
				p1++
			}
			hasMatchCount = p2
		}
		if hasMatchCount == len(str2) {
			return p1
		}

	}

	return -1

}
func getNext(str string) []int {
	var next []int
	for i := 1; i <= len(str); i++ {
		max := 0
		for j := 0; j < i; j++ {
			frontStr := str[0:j]
			backStr := str[i-j : i]
			if backStr == frontStr {
				if max < len(backStr) {
					max = len(backStr)
				}
			}
		}
		//fmt.Printf("%s,%v", str[0:i], max)
		//fmt.Println("")
		//fmt.Println("------")
		next = append(next, max)

	}
	return next
}
