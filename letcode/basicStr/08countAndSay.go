package main

import (
	"fmt"
	"strconv"
)

func main() {
	say := countAndSay(5)
	fmt.Println(say)
}

func countAndSay(n int) string {
	if n == 1 {
		return "1"
	}
	say := countAndSay(n - 1)

	var str string
	start := 0
	count := 0
	for i := start; i < len(say); i++ {
		if say[start] == say[i] {
			count++
		} else {
			str += strconv.Itoa(count) + string(say[start])
			count = 0
			start = i
			i--
		}
	}
	str += strconv.Itoa(count) + string(say[start])

	return str
}
