package main

import "fmt"

func main() {
	 arr:=[]byte{1,2,3,4,5}
	 reverseString(arr)
	 fmt.Print(arr)
}

func reverseString(s []byte)  {
	n:=len(s)
	for i := 0; i < (n+1)/2; i++ {
		temp:=s[i]
		s[i]=s[n-i-1]
		s[n-i-1]=temp
	}
}
