package main

import (
	"math"
	"strings"
)

func main() {

}

func myAtoi(str string) int {
	str = strings.Trim(str, " ")
	if str == "" {
		return 0
	}

	flag := 1
	tstr := 0
	starIndex := 0
	if str[0] == '-' {
		flag = -1
		starIndex++
	}
	if str[0] == '+' {
		flag = 1
		starIndex++
	}

	for i := starIndex; i < len(str); i++ {
		if str[i] == '.' {
			return tstr * flag
		}
		if str[i] < '0' || str[i] > '9' {
			return tstr * flag
		}
		v := int(str[i] - '0')
		if tstr*flag > math.MaxInt32/10 || (tstr*flag == math.MaxInt32/10 && v*flag > 7) {
			return math.MaxInt32 * flag
		}
		if tstr*flag < math.MinInt32/10 || (tstr*flag == math.MinInt32/10 && v*flag < -8) {
			return math.MinInt32
		}
		tstr = tstr*10 + v
	}
	return tstr * flag

}
