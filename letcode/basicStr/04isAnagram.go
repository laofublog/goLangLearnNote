package main

import "fmt"

func main() {
	result := isAnagram("aa", "bb")
	fmt.Println(result)
}

func isAnagram(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}
	m := make(map[byte]int)
	for i := 0; i < len(s); i++ {
		if _, hasKey := m[s[i]]; hasKey {
			m[s[i]]++
		} else {
			m[s[i]] = 1
		}
		if _, hasKey := m[t[i]]; hasKey {
			m[t[i]]--
		} else {
			m[t[i]] = -1
		}
	}

	for _, v := range m {
		if v!= 0 {
			return false
		}
	}
	return true
}
