package main

import "fmt"

func main() {
	str := []string{"aca", "cba"}
	prefix := longestCommonPrefix(str)
	fmt.Println(prefix)
}

func longestCommonPrefix(strs []string) string {

	if len(strs) == 0 {
		return ""
	}
	l := len(strs[0])
	index := 0
	for i := 0; i < l; i++ {
		c := strs[0][i]
		b := false
		match := true
		for j := 1; j < len(strs); j++ {
			if len(strs[j]) < i+1 {
				b = true
				match = false
				break
			}
			if strs[j][i] != c {
				if i == 0 {
					b = true
				}
				match = false
			}
		}
		if b {
			break
		}
		if match {
			index++
		}
	}

	return strs[0][0:index]

}
