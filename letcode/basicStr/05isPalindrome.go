package main

import (
	"fmt"
)

func main() {
	s := "0P"
	palindrome := isPalindrome(s)
	fmt.Println(palindrome)
}
func isPalindrome(s string) bool {
	n := len(s)
	var str []byte
	for i := 0; i < n; i++ {
		if s[i] >= 'A' && s[i] <= 'Z' {
			str = append(str, s[i]+32)
		}
		if s[i] >= 'a' && s[i] <= 'z' {
			str = append(str, s[i])
		}

		if s[i] >= '0' && s[i] <= '9' {
			str = append(str, s[i])
		}
	}

	for i := 0; i < (len(str))/2; i++ {
		if str[i] != str[len(str)-i-1] {
			return false
		}
	}
	return true
}
