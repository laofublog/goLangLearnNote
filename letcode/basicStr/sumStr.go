package main

import (
	"fmt"
	"strconv"
)

func main() {
	str := sumStr("2222", "644444")//64666
	fmt.Println(str)

}

func sumStr(s1, s2 string) string {
	flag := 0
	len1 := len(s1)
	len2 := len(s2)
	max := len1
	var result string
	if len2 > max {
		max = len2
	}

	for i := max - 1; i >= 0; i-- {
		if len1 > i && len2 > i {
			sum := (int(s1[len1-i-1]) - '0') + (int(s2[len2-i-1]) - '0') + flag
			if sum > 9 {
				flag = 1
				sum = sum % 10
			}
			result = strconv.Itoa(sum) + result
		}
	}
	if len1 == max {
		result = s1[0:max-len2] + result
	}
	if len2 == max {
		result = s2[0:max-len1] + result
	}
	return result
}
