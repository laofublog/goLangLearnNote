package main

import "fmt"

func main() {
	char := firstUniqChar("aadadaad")
	fmt.Print(char)
}
func firstUniqChar(s string) int {
	m := make(map[byte]int)
	var arr []int
	for i := 0; i < len(s); i++ {
		if index, hasKey := m[s[i]]; hasKey {
			arr[index] = 1
			arr = append(arr, 1)
		} else {
			m[s[i]] = i
			arr = append(arr, 0)
		}
	}
	for i := 0; i < len(arr); i++ {
		if arr[i] == 0 {
			return i
		}
	}
	return -1

}
