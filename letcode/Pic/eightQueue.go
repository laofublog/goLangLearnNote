package main

import (
	"fmt"
	"math"
)

func main() {
	arr :=  [][]int{}
	path := make([]int, 0)
	solveNQueens(arr, 8, path)
	fmt.Println(arr)

}

func solveNQueens(arr [][]int, n int, path []int) {
	if len(path) == n {
		arr = append(arr, path)
		return
	}
	for i := 0; i < n; i++ {
		if check(path, i) {
			path = append(path, i)
			solveNQueens(arr, n, path)
			path = path[:len(path)-1]
		}
	}
}

func check(path []int, x int) bool {
	for i := 0; i < len(path); i++ {
		if path[i] == x {
			return false
		}
		if math.Abs(float64(x-path[i])) == math.Abs(float64(len(path)-i)) {
			return false
		}
	}
	return true
}
func max(v1, v2 int) int {
	if v1 > v2 {
		return v1
	}
	return v2
}
