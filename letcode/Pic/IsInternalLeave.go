package main

import "fmt"

func main() {
	s1, s2, s3 := "aabcc", "dbbca", "aadbbcbcac"
	leave := isInternalLeave(s1, s2, s3)
	fmt.Println(leave)
}

func isInternalLeave(s1, s2, s3 string) bool {
	l1, l2, l3 := len(s1), len(s2), len(s3)
	if l1+l2 != l3 {
		return false
	}

	var dp [][]int
	for i := 0; i < l1; i++ {
		var tmp []int
		for j := 0; j < l2; j++ {
			tmp = append(tmp, 0)
		}
		dp = append(dp, tmp)
	}

	for i := 0; i < l1; i++ {
		for j := 0; j < l2; j++ {
			if i == 0 && j == 0 {
				dp[i][j] = 1
			} else if i == 0 {
				if dp[i][j-1] == 1 && s2[j-1] == s3[i+j-1] {
					dp[i][j] = 1
				}
			} else if j == 0 {
				if dp[i-1][j] == 1 && s1[i-1] == s3[i+j-1] {
					dp[i][j] = 1
				}

			} else if (dp[i-1][j] == 1 && s1[i-1] == s3[i+j-1]) || (dp[i][j-1] == 1 && s2[j-1] == s3[i+j-1]) {
				dp[i][j] = 1
			} else {
				dp[i][j] = 0
			}

		}
	}
	fmt.Println(dp)
	return dp[l1-1][l2-1]==1

}
