package main

import "fmt"

func main() {

	i := partition("aab")
	fmt.Println(i)
}

func partition(str string) [][]string {
	resultArr := make([][]string, 0)
	var pathArr []string
	dfs(str, pathArr, resultArr, 0)
	return resultArr
}

func dfs(s string, path []string, result [][]string, start int) {
	if start == len(s) {
		result = append(result, path)
		fmt.Println(path)
		return
	}

	for i := start; i < len(s); i++ {
		//从 i 到 start 如果是回文
		if isPalindrome(s, start, i) {
			//追加到 path
			path = append(path, s[start:i+1])
			//然后从 i+1 开始判断 是否是回文
			dfs(s, path, result, i+1)
			//1. 如果 0~0 已经是回文了
			//2. 需要判断 0~1 是否是回文，所以此处需要移除 0~1 的结果
			path = path[:len(path)-1]
		}
	}
}

func isPalindrome(s string, start, end int) bool {
	for start < end && s[start] == s[end] {
		start++
		end--
	}
	return start >= end
}
