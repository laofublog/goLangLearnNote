package main

import "fmt"

func main() {
	arr := [][]int{
		{1, 1, 1, 1},
		{1, 0, 0, 1},
		{1, 1, 0, 1},
		{1, 0, 1, 1},
	}
	solve(arr)
	fmt.Println(arr)
}

func solve(arr [][]int) {
	m := len(arr)
	n := len(arr[0])
	for i := 0; i < m; i++ {
		bfs(arr, 0, i)
		bfs(arr, m-1, i)
	}
	for j := 0; j < n; j++ {
		bfs(arr, 0, j)
		bfs(arr, n-1, j)
	}

	fmt.Println(arr)
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if arr[i][j] == 0 {
				arr[i][j] = 1
			} else if arr[i][j] == 2 {
				arr[i][j] = 0
			}
		}
	}
}

func bfs(eArr [][]int, i, j int) {

	var queue [][]int

	if isValid(eArr, i, j) {
		eArr[i][j] = 2

		queue = append(queue, []int{i, j})
	}
	for len(queue) > 0 {
		tmp := queue[len(queue)-1]
		queue = queue[0 : len(queue)-1]
		result := extend(eArr, tmp[0], tmp[1])
		for i := 0; i < len(result); i++ {
			queue = append(queue, result[i])
		}
	}
}

func extend(eArr [][]int, i, j int) [][]int {
	var result [][]int
	path := [][]int{
		{i - 1, j},
		{i, j - 1},
		{i + 1, j},
		{i, j + 1},
	}
	for i := 0; i < len(path); i++ {
		if isValid(eArr, path[i][0], path[i][1]) {
			result = append(result, []int{i, j})
		}
	}
	return result
}
func isValid(eArr [][]int, i, j int) bool {
	m := len(eArr)
	n := len(eArr[0])
	if i < 0 || i >= m || j < 0 || j >= n || eArr[i][j] != 0 {
		return false
	}
	return true
}
