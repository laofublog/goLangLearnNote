package main

import "fmt"

func main() {
	arr := []int{7, 1, 5, 3, 6, 4}
	profit := maxProfit(arr)
	fmt.Println(profit)
}

func maxProfit(prices []int) int {
	if len(prices) < 1 {
		return 0
	}
	min := prices[0]
	maxprofit := 0
	for i := 1; i < len(prices); i++ {
		if min > prices[i] {
			min = prices[i]
		} else if prices[i]-min > maxprofit {
			maxprofit = prices[i] - min
		}

	}
	return maxprofit
}
