package main

import "fmt"

func main() {
	arr := []int{2, 1, 1, 2}
	i := rob(arr)
	fmt.Println(i)

}
func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	if len(nums) == 2 {
		return maxValue(nums[0], nums[1])
	}
	var arr []int
	arr = append(arr, nums[0])
	arr = append(arr, maxValue(nums[0], nums[1]))

	for i := 2; i < len(nums); i++ {
		arr = append(arr, maxValue(arr[i-1], arr[i-2]+nums[i]))
	}
	return arr[len(arr)-1]
}

func maxValue(v1, v2 int) int {
	if v1 > v2 {
		return v1
	}
	return v2
}
