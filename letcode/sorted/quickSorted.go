package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 5, 6, 7, 8, 5, 4, 5, 6, 7, 3, 2, 3, 45, 5, 2}
	fmt.Println(arr)
	quickSortFunc(&arr, 0, len(arr)-1)
	fmt.Println(arr)
}

func quickSorted(arr []int) {

}

func quickSortFunc(pArr *[]int, left, right int) {
	values := (*pArr)
	temp := values[left]
	p := left
	i, j := left, right

	for i <= j {
		for j >= p && values[j] >= temp {
			j--
		}
		if j >= p {
			values[p] = values[j]
			p = j
		}

		for i <= p && values[i] <= temp {
			i++
		}
		if i <= p {
			values[p] = values[i]
			p = i
		}
	}
	values[p] = temp
	if p-left > 1 {
		quickSortFunc(pArr, left, p-1)
	}
	if right-p > 1 {
		quickSortFunc(pArr, p+1, right)
	}

}
