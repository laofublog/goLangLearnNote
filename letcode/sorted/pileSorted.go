package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 5, 6, 7, 8, 5, 4, 5, 6, 7, 3, 2, 3, 45, 5, 2}
	fmt.Println(arr)
	pileSorted(arr)
	fmt.Println(arr)
}

func pileSorted(arr []int) {
	size := len(arr)
	for i := size/2 - 1; i >= 0; i-- {
		adjustPile(arr, i)

	}
	for len(arr) > 1 {
		arr[len(arr)-1], arr[0] = arr[0], arr[len(arr)-1]
		arr = arr[:len(arr)-1]
		adjustPile(arr, 0)
	}

}

func adjustPile(arr []int, n int) {
	lChild := 2*n + 1
	size := len(arr)
	for lChild < size {
		if lChild+1 < size && arr[lChild+1] > arr[lChild] {
			lChild++
		}
		if arr[lChild] < arr[n] {
			break
		}
		arr[lChild], arr[n] = arr[n], arr[lChild]
		n = lChild
		lChild = 2*n + 1
	}

}
