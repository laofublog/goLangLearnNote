package main

import (
	"fmt"
	"math/rand"
)

type Solution struct {
	arr     []int
	backArr []int
}

func Constructor1(nums []int) Solution {
	solution := Solution{arr: nums}
	solution.backArr = append(solution.backArr, nums...)
	return solution
}

/** Resets the array to its original configuration and return it. */
func (this *Solution) Reset() []int {
	copy(this.arr, this.backArr)
	return this.backArr
}

/** Returns a random shuffling of the array. */
func (this *Solution) Shuffle() []int {
	len := len(this.arr)
	for i := 0; i < len; i++ {
		index := rand.Intn(len-i) + i
		this.arr[i], this.arr[index] = this.arr[index], this.arr[i]
	}
	return this.arr
}

/**
 * Your Solution object will be instantiated and called as such:
 * obj := Constructor(nums);
 * param_1 := obj.Reset();
 * param_2 := obj.Shuffle();
 */

func main() {

	arr := []int{1, 2, 3}
	constructor := Constructor1(arr)
	for i := 0; i < 100; i++ {
		//fmt.Println(rand.Intn(i + 1))
		fmt.Println(constructor.Shuffle())
		fmt.Println(constructor.Reset())
		fmt.Println(constructor.Shuffle())
	}
	fmt.Println(constructor.Shuffle())
	fmt.Println(constructor.Reset())
	fmt.Println(constructor.Shuffle())
}
