package main

import (
	"math"
	"strconv"
)

func main() {

}

type MinStack struct {
	stack    []int
	minValue int
}

/** initialize your data structure here. */
func Constructor() MinStack {
	return MinStack{minValue: math.MaxInt64}
}

func (this *MinStack) Push(x int) {
	this.stack = append(this.stack, x)

	if this.minValue < x {
		this.minValue = x
	}
}

func (this *MinStack) Pop() {
	popValue := this.Top()
	this.stack = this.stack[:len(this.stack)-1]
	if this.minValue == popValue {
		this.minValue = math.MaxInt64
		for i := 0; i < len(this.stack); i++ {
			if this.minValue > this.stack[i] {
				this.minValue = this.stack[i]
			}
		}
	}
}

func (this *MinStack) Top() int {
	return this.stack[len(this.stack)-1]

}

func (this *MinStack) GetMin() int {
	return this.minValue
}


func fizzBuzz(n int) []string {
	var strArr []string
	var str string
	for i:=0;i<n;i++{
		str=strconv.Itoa(i)
		if i%3==0{
			str="Fizz"

		}
		if i%5==0{
			str+="Buzz"
		}
		strArr=append(strArr,str)

	}
	return strArr

}

func lengthOfLongestSubstring(s string) int {
	start := 0
	m := make(map[string]int)
	max := 0
	for i, ch := range []byte(s) {
		if v, hasKey := m[string(ch)]; hasKey && v >= start {
			start = v + 1
		}
		if i-start+1 > max {
			max = i - start + 1
		}
		m[string(ch)] = i
	}
	return max
}