package main

import "fmt"

func main() {
	nums1 :=[]int {4,9,5}
	nums2 := []int{9,4,9,8,4}
	ints := intersect(nums1, nums2)
	fmt.Println(ints)
}


func intersect(nums1 []int, nums2 []int) []int {
	lnums:=nums1
	snums:=nums2
	if len(nums1)<len(nums2){
		lnums=nums2
		snums=nums1
	}
	m := make(map[int] int)
	var arr [] int
	for i := 0; i < len(snums); i++ {
		m[snums[i]]++
	}

	for i := 0; i < len(lnums); i++ {
		if v,hasKey:=m[lnums[i]];hasKey{
			if v!=0{
				arr=append(arr,lnums[i])
				m[lnums[i]]--
			}
		}
	}
	return arr
}