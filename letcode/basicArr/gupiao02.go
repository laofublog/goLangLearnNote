package main

import "fmt"

func main() {
	price:=[]int{7,1,5,3,6,4}
	arr := getPrice(price)
	fmt.Println(arr)
}


func getPrice(prices []int) int{
	if len(prices)<2{
		return 0
	}
	if len(prices)==2 {
		income:=prices[1]-prices[0]
		if income<=0 {
			return 0
		}
		return income
	}

    total:=0
	buyPrice:=prices[0]
	for i := 1; i < len(prices); i++ {
		income := prices[i] - buyPrice
		if income<=0 {
			buyPrice=prices[i]
			continue
		}
		income+=getPrice(prices[i+1:])
		if income>total{
			total=income
		}
	}
	return total
}