package main

import "C"
import (
	"fmt"
	"strconv"
)

func main() {
	var arr []string
	for i := 1; i < 100; i++ {
		arr = append(arr, "A"+strconv.Itoa(i))
	}
	for i := 1; i < 100; i++ {
		arr = append(arr, "B"+strconv.Itoa(i))
	}
	fmt.Println(arr)
	swapPosition(&arr)
	fmt.Println(arr)

}

func swapMedium(arr *[]string) {
	p := *arr
	size := len(p) / 2
	index := 0
	for i := size; i < len(p)-1; i++ {
		//i-size 代表第几次交换
		for j := 0; j <= index; j += 2 {
			//开始位置
			offset := size - (i - size) + j - 1
			swapm(arr, offset, offset+1)
		}
		index += 2
	}
}

func swapPosition(arr *[]string) {
	n := len(*arr) / 2
	p := *arr
	for n >= 1 {
		k := 0
		r := 3
		for r-1 <= 2*n {
			r *= 3
			k++
		}
		//2m=3^k-1
		//1. 找到M
		m := (r/3 - 1) / 2

		//2. 左移
		rArr := p[m : n+m]
		leftRotate(&rArr, m)

		//3. 走环
		start := 1
		for i := 0; i < k; i++ {
			cycleLeader(&p, start, m)
			start *= 3
		}

		//4. 剩余的数组
		p = p[2*m:]
		n = n - m

	}

}

// 走圈算法
func cycleLeader(arr *[]string, start, n int) {
	//p := *arr
	mod := 2*n - 1
	//pre := p[start]
	next := (2 * start) % mod
	//next ==0 代表不用移动的元素
	for start != next && next != 0 {
		swapm(arr, start, next)
		next = (2 * next) % mod

	}

}

func reverse(arr *[]string, start, end int) {
	for start < end {
		swapm(arr, start, end)
		start++
		end--
	}
}
func leftRotate(arr *[]string, m int) {
	reverse(arr, 0, len(*arr)-1)
	reverse(arr, m, len(*arr)-1)
	reverse(arr, 0, m-1)
}

func swapm(arr *[]string, i1, i2 int) {
	tmp := (*arr)[i1]
	(*arr)[i1] = (*arr)[i2]
	(*arr)[i2] = tmp
}
