package main

import "fmt"

func main() {
	nums := []int{9}
	nums=plusOne(nums)
	fmt.Println(nums)
}

func plusOne(digits []int) []int {
	flag := 1
	for i := len(digits) - 1; i >= 0; i-- {
		v := (digits[i] + flag) % 10
		flag = (digits[i] + flag) / 10
		digits[i] = v
		if flag == 0 {
			return digits
		}
	}
	if flag == 1 {
		digits=append([]int{1},digits...)
	}
	return digits

}
