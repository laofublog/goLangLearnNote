package main

import "fmt"

func main() {
	nums := []int{2, 7, 11, 15}
	result:=twoSum(nums,9)
	fmt.Println(result)
}


func twoSum(nums []int, target int) []int {

	for i := 0; i < len(nums); i++ {
		value:=target-nums[i]
		for j := i+1; j < len(nums); j++ {
			if value==nums[j]{
				return []int{i,j}
			}
		}
	}
	return []int{}

}