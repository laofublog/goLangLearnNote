package main

import "fmt"

func main() {
	nums := []int{0, 1, 0, 3, 12}
	moveZeroes(nums)
	fmt.Println(nums)
}

func moveZeroes(nums []int) {
	for i := 0; i < len(nums); i++ {
		if nums[i] != 0 {
			continue
		}
		for j := i+1; j < len(nums); j++ {
			if nums[j]==0 {
				continue
			}
			temp:=nums[i]
			nums[i]=nums[j]
			nums[j]=temp

		}
	}
}
