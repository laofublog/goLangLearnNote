package main

import "fmt"

func main() {
	numArr := [][]int{
		{5, 1, 9, 11},
		{2, 4, 8, 10},
		{13, 3, 6, 7},
		{15, 14, 12, 16},
	}
	rotateMatrix(numArr)
}

//[
//[15,13, 2, 5],
//[14, 3, 4, 1],
//[12, 6, 8, 9],
//[16, 7,10,11]
//]

func rotateMatrix(matrix [][]int) {

	n := len(matrix)
	for i := 0; i < (n + 1) / 2; i++ {
		for j := 0; j < n/2; j++ {
			temp := matrix[i][j]
			matrix[i][j] = matrix[n-j-1][i]
			matrix[n-j-1][i] = matrix[n-i-1][n-j-1]
			matrix[j][n-i-1] = temp
		}
	}
	fmt.Println(matrix)
}
