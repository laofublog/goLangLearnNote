package main

import "fmt"

func main() {
	numArr := [][]byte{
		{'.','.','.'  ,'.','5', '.',  '.','1','.'},
		{'.','4','.'  ,'3','.', '.',  '.','.','.'},
		{'.','.','.'  ,'.','.', '3',  '.','.','1'},

		{'8','.','.'  ,'.','.', '.',  '.','2','.'},
		{'.','.','2'  ,'.','7', '.',  '.','.','.'},
		{'.','1','5'  ,'.','.', '.',  '.','.','.'},

		{'.','.','.'  ,'.','.', '2',  '.','.','.'},
		{'.','2','.'  ,'9','.', '.',  '.','.','.'},
		{'.','.','4'  ,'.','.', '.',  '.','.','.'},
	}


	sudoku := isValidSudoku1(numArr)
	fmt.Println(sudoku)
}

func isValidSudoku(board [][]byte) bool {
	var row,col,sbox [9][9]int
	for i := 0 ; i < 9; i++ {
		for j := 0 ; j < 9; j++ {
			if board[i][j] != '.' {
				//代表当前的索引
				num := board[i][j]-'1'
				index_box := (i/3)*3+j/3
				if row[i][num] == 1 {
					return false
				}else{
					row[i][num] = 1
				}
				if col[j][num] == 1 {
					return false
				}else{
					col[j][num] = 1
				}
				if sbox[index_box][num] == 1 {
					return false
				}else {
					sbox[index_box][num] = 1
				}
			}
		}
	}
	return true
}

func isValidSudoku1(board [][]byte) bool {
	for i := 0; i < 9; i++ {
		container := make(map[byte]byte)
		for j := 0; j < 9; j++ {
			if board[i][j] == '.' {
				continue
			}
			if _, hasKey := container[board[i][j]]; hasKey {
				return false
			}
			container[board[i][j]] = board[i][j]
		}
	}
	for j := 0; j < 9; j++ {
		container := make(map[byte]byte)
		for i := 0; i < 9; i++ {
			if board[i][j] == '.' {
				continue
			}
			if _, hasKey := container[board[i][j]]; hasKey {
				return false
			}
			container[board[i][j]] = board[i][j]
		}
	}
	return true
}
