package main

import "fmt"

func main() {
	var arr []int
	var pb []int
	positive := 0
	negative := 0
	for i := -10; i < 0; i++ {
		arr = append(arr, i)
		pb = append(pb, 0)
		negative += i
	}
	for i := 1; i < 11; i++ {
		arr = append(arr, i)
		pb = append(pb, 0)
		positive += i
	}
	sum := 40
	findNumberWithNegative(&arr, &pb, 0, 0, negative, positive, sum)

}

// residue 代表 i...n 数字的总和
func findNumber(arr *[]int, pb *[]int, i, has, residue, sum int) {
	if i >= len(*arr) {
		return
	}
	p := *arr
	x := *pb
	if has+p[i] == sum {
		x[i] = p[i]
		fmt.Println(x)
		x[i] = 0
	} else if has+residue >= sum && has+p[i] <= sum {
		//如果当前值
		x[i] = p[i]
		findNumber(arr, pb, i+1, has+p[i], residue-p[i], sum)
	}
	if has+residue-p[i] >= sum {
		x[i] = 0
		findNumber(arr, pb, i+1, has, residue-p[i], sum)
	}
}

func findNumberWithNegative(arr *[]int, pb *[]int, i, has, negative, positive, sum int) {
	if i >= len(*arr) {
		return
	}
	p := *arr
	x := *pb
	if has+p[i] == sum {
		x[i] = p[i]
		fmt.Println(x)
		x[i] = 0
	}
	if p[i] >= 0 {
		//01. 此处都是正数

		//如果当前has +r 大于总数，再增加一个元素 p[i] 小于 sum.说明当值可以需要
		if has+positive >= sum && has+p[i] <= sum {
			//如果当前值
			x[i] = p[i]
			findNumberWithNegative(arr, pb, i+1, has+p[i],negative, positive-p[i], sum)
			x[i] = 0
		}
		//如果 has+r 再减去当前元素大于 sum,说明当前元素不合适
		if has+positive-p[i] >= sum {
			x[i] = 0
			findNumberWithNegative(arr, pb, i+1, has,negative, positive-p[i], sum)
		}
	} else {
		//02. 都是负数

		//如果 has+r+正数的值大于 sum, 所以当前元素需要。
		if has+p[i]+positive >= sum {
			x[i] = p[i]
			findNumberWithNegative(arr, pb, i+1, has+p[i], negative-p[i], positive,sum)
			x[i] = 0
		}
		// 如果当前 has+positive >=sum  说明正数溢出
		// 如果has+negative<=sum 说明 p[i]已经无法满足
		if has+positive >= sum && has+negative <= sum {
			x[i] = 0
			findNumberWithNegative(arr, pb, i+1, has, negative-p[i], positive,sum)
		}
	}
}
