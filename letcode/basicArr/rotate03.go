package main

import "fmt"

func main() {
	nums := []int{-1, 2}
	k := 3
	rotate(nums, k)
	fmt.Println(nums)

}
func rotate(nums []int, k int) {
	if k == 0 || len(nums) < 2 {
		return
	}
	if k > len(nums) {
		k = k % len(nums)
	}
	backArr := make([]int, k)
	copy(backArr, nums[len(nums)-k:])
	copy(nums[k:], nums[0:len(nums)-k])
	copy(nums[0:k], backArr)
}
