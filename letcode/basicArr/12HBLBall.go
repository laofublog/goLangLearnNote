package main

import (
	"fmt"
	"math/rand"
)

func main() {

	var arr []int
	for i := 0; i < 50; i++ {
		arr = append(arr, rand.Intn(3))
	}
	result := hblBall(&arr)
	fmt.Println(result)

}

func hblBall(arr *[]int) []int {
	p := *arr
	begin := 0
	current := 0
	end := len(p) - 1

	for current <= end {
		if p[current] == 2 {
			swap(arr, current, end)
			end--
			continue
		}
		if p[current] == 1 {
			current++
			continue
		}
		if p[current] == 0 {
			if begin == current {
				begin++
				current++
			} else {
				swap(arr, current, begin)
				begin++
				current++
			}
		}

	}
	return p
}

func swap(arr *[]int, i1, i2 int) {
	tmp := (*arr)[i1]
	(*arr)[i1] = (*arr)[i2]
	(*arr)[i2] = tmp
}
