package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5,-5,-2}
	var pd []int
	for i := 0; i < len(arr); i++ {
		pd = append(pd, 0)
	}
	mSum1(&arr, &pd, 10, 0)

}
func mSum1(arr *[]int, pb *[]int, sum, start int) {
	p := *arr
	x := *pb
	if start >= len(p) {
		return
	}
	if sum == p[start] {
		x[start] = p[start]
		fmt.Println(*pb)
		x[start] = 0
	}
	x[start] = p[start]
	mSum1(arr, pb, sum-p[start], start+1)
	x[start] = 0
	mSum1(arr, pb, sum, start+1)


}

func mSum(arr *[]int, pb *[]bool, i, has, sum int) {
	if i >= len(*arr) {
		return
	}
	p := *arr
	x := *pb
	if has+p[i] == sum {
		x[i] = true
		fmt.Println(x)
		x[i] = false
	}
	x[i] = true
	mSum(arr, pb, i+1, has+p[i], sum)
	x[i] = false
	mSum(arr, pb, i+1, has, sum)
}
