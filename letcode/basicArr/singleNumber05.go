package main

import "fmt"

func main() {
	nums := []int{4,1,2,1,2}
	duplicate := singleNumber(nums)
	fmt.Println(duplicate)
}
func singleNumber(nums []int) int {
	 ans:=nums[0]
	for i := 1; i < len(nums); i++ {
		 ans^=nums[i]
	}
	return ans
}
