package main

import "fmt"

func main() {
	arr1 := []int{4, 0, 0, 0, 0, 0}
	arr2 := []int{1, 2, 3, 5, 6}
	merge(arr1, 1, arr2, 5)
	fmt.Println(arr1)

}
func merge(nums1 []int, m int, nums2 []int, n int) {
	if m == 0 {
		for i := 0; i < n; i++ {
			nums1[i] = nums2[i]
		}
		return
	}
	if n == 0 {
		return
	}
	for i := 0; i < m || i < n; i++ {
		if nums1[m+n-i-1] < nums2[n-i-1] {
			nums1[m+n-i-1] = nums2[n-i-1]
		} else {
			nums1[m+n-i-1] = nums1[m-i-1]
		}

	}

}
