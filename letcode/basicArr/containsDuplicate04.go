package main

import "fmt"

func main() {
	nums := []int{1, 1, 1, 3, 3, 4, 3, 2, 4, 2}
	duplicate := containsDuplicate(nums)
	fmt.Println(duplicate)
}
func containsDuplicate(nums []int) bool {
	container := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		if _, hasKey := container[nums[i]]; hasKey {
			return true
		}
		container[nums[i]]=nums[i]
	}
	return false
}
