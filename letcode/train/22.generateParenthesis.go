package main

import "fmt"

func main() {
	fmt.Println(generateParenthesis(1))
	fmt.Println(generateParenthesis(2))
	fmt.Println(generateParenthesis(3))
	fmt.Println(generateParenthesis(4))
	fmt.Println(generateParenthesis(5))
}

func generateParenthesis(n int) []string {
	var arr [][]string
	arr = append(arr, []string{"()"})
	if n == 1 {
		return arr[0]
	}
	m := make(map[string]byte)
	for i := 1; i < n; i++ {
		var result []string
		for j := 0; j < len(arr[i-1]); j++ {
			curArr := arr[i-1][j]
			for k := 0; k < len(curArr); k++ {
				valResult := curArr[:k] + "()" + curArr[k:]
				if _, hasKey := m[valResult]; hasKey {
					continue
				}
				m[valResult] = 1
				result = append(result, valResult)
			}
		}
		arr = append(arr, result)
	}
	return arr[len(arr)-1]

}
