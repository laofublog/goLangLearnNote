package main

import "fmt"

func main() {
	fmt.Println(searchInsert([]int{1, 3, 5, 6}, 5))
	fmt.Println(searchInsert([]int{1, 3, 5, 6}, 2))
	fmt.Println(searchInsert([]int{1, 3, 5, 6}, 7))
	fmt.Println(searchInsert([]int{1, 3, 5, 6}, 0))
	fmt.Println(searchInsert([]int{1}, 1))
	fmt.Println(searchInsert([]int{1, 3}, 3))

}

func searchInsert(nums []int, target int) int {
	if nums[0] >= target {
		return 0
	}
	for i := 0; i < len(nums); i++ {
		if nums[i] == target {
			return i
		}
		if nums[i] < target && i < len(nums)-1 && nums[i+1] > target {
			return i + 1
		}
	}
	return len(nums)

}
