package main

import (
	"fmt"
)

func main() {
	fmt.Println(searchRange([]int{2, 2}, 2))
	fmt.Println(searchRange([]int{5, 7, 7, 8, 8, 10}, 8))
	fmt.Println(searchRange([]int{5, 7, 7, 8, 8, 10}, 6))
	fmt.Println(searchRange([]int{1, 4}, 4))
}

func searchRange(nums []int, target int) []int {
	if len(nums) == 0 {
		return []int{-1, -1}
	}
	start := findFirst(nums, target)
	if start == -1 {
		return []int{-1, -1}
	}
	end := findLast(nums, target)
	return []int{start, end}
}

func findFirst(nums []int, target int) int {
	start, end := 0, len(nums)-1
	for start < end {
		mid := (start + end) >> 1
		if nums[mid] > target {
			end = mid - 1
		} else if nums[mid] == target {
			end = mid
		} else {
			start = mid + 1
		}
	}
	if nums[start] == target {
		return start
	}
	return -1
}

func findLast(nums []int, target int) int {
	start, end := 0, len(nums)-1
	for start < end {
		mid := (start + end + 1) >> 1
		if nums[mid] > target {
			end = mid - 1
		} else if nums[mid] == target {
			start = mid
		} else {
			start = mid + 1
		}
	}

	return end
}
