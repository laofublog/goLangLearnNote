package main

import "fmt"

func main() {

	arr := [][]int{{1, 4, 5}, {0, 3, 4}, {2, 6}}
	lists := ConvertLinkList(arr)
	fmt.Println(lists)
	kLists := mergeKLists(lists)
	fmt.Println(kLists)
}

func mergeKLists(lists []*ListNode) *ListNode {
	if len(lists) == 0 {
		return nil
	}
	p := lists[0]
	for i := 1; i < len(lists); i++ {
		p = mergerTwo(p, lists[i])
	}
	return p

}

func mergerTwo(node1, node2 *ListNode) *ListNode {
	if node1 == nil {
		return node2
	}
	if node2 == nil {
		return node1
	}
	p := &ListNode{}
	if node2.Val < node1.Val {
		p.Next = node2
		node2 = node2.Next
	} else {
		p.Next = node1
		node1 = node1.Next
	}
	head := p
	p = p.Next
	for node1 != nil && node2 != nil {
		if node2.Val <= node1.Val {
			p.Next = node2
			node2 = node2.Next
		} else {
			p.Next = node1
			node1 = node1.Next
		}
		p = p.Next
	}
	if node1 != nil {
		p.Next = node1
	} else {
		p.Next = node2
	}
	return head.Next
}

func ConvertLinkList(arr [][]int) []*ListNode {
	var lists []*ListNode

	for i := 0; i < len(arr); i++ {
		header := &ListNode{Val: arr[i][0]}
		p := header
		for j := 1; j < len(arr[i]); j++ {
			p.Next = &ListNode{Val: arr[i][j]}
			p = p.Next
		}
		lists = append(lists, header)
	}
	return lists
}