package main

import (
	"fmt"
)

func main() {
	arr := [][]byte{
		{'5', '.', '4', '.', '3', '.', '8', '.', '.'},
		{'.', '2', '1', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '6', '.', '.', '.', '.'},
		{'4', '1', '8', '3', '.', '.', '9', '7', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '4', '6'},
		{'.', '.', '.', '5', '4', '.', '.', '.', '.'},
		{'.', '.', '5', '6', '9', '.', '.', '2', '.'},
		{'6', '.', '.', '2', '8', '.', '.', '9', '.'},
		{'.', '3', '9', '.', '1', '.', '.', '.', '.'},
	}
	solveSudoku(arr)
	//fmt.Println(arr)
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[0]); j++ {
			fmt.Print(arr[i][j]-48, " ")
		}
		fmt.Println("")
	}

}

func solveSudoku(arr [][]byte) bool {
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if arr[i][j] == '.' {
				for k := 1; k < 10; k++ {
					arr[i][j] = byte(k + 48)
					if isValide(arr, i, j) && solveSudoku(arr) {
						return true
					}
					arr[i][j] = '.'
				}
				return false
			}
		}
	}
	return true
}

func isValide(arr [][]byte, x, y int) bool {
	for i := 0; i < 9; i++ {
		if i != x && arr[i][y] == arr[x][y] {
			return false
		}

	}
	for j := 0; j < 9; j++ {
		if j != y && arr[x][j] == arr[x][y] {
			return false
		}
	}
	for i := 3 * (x / 3); i < 3*(x/3+1); i++ {
		for j := 3 * (y / 3); j < 3*(y/3+1); j++ {
			if (i != x || j != y) && arr[i][j] == arr[x][y] {
				return false
			}
		}

	}
	return true
}
