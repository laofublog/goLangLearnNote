package main

func main() {

}

func twoSum(nums []int, target int) []int {
	for i := 0; i < len(nums); i++ {
		value := target - nums[i]
		for j := i + 1; j < len(nums); j++ {
			if value == nums[j] {
				return []int{i, j}
			}
		}
	}
	return []int{}

}
