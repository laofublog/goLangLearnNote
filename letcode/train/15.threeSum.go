package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(threeSum([]int{-1, 0, 1, 2, -1, -4}))
}

func threeSum(nums []int) [][]int {
	var arr [][]int
	if len(nums) < 3 {
		return arr
	}
	sort.Ints(nums)
	if nums[0] > 0 || nums[len(nums)-1] < 0 {
		return arr
	}

	for start := 0; start < len(nums); start++ {
		//防止重复解
		if start > 0 && nums[start-1] == nums[start] {
			continue
		}
		end := len(nums) - 1
		for mid := start + 1; mid < len(nums); mid++ {
			if mid > start+1 && nums[mid] == nums[mid-1] {
				continue
			}
			for mid < end && nums[start]+nums[mid]+nums[end] > 0 {
				end--
			}
			if mid == end {
				break
			}
			if nums[start]+nums[mid]+nums[end] == 0 {
				arr = append(arr, []int{nums[start], nums[mid], nums[end]})
			}
		}

	}
	return arr

}
