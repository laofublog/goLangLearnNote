package main

import "fmt"

func main() {
	fmt.Println(letterCombinationsdp("234"))
	fmt.Println(letterCombinations("234"))
}

var phoneMap map[string]string = map[string]string{
	"2": "abc",
	"3": "def",
	"4": "ghi",
	"5": "jkl",
	"6": "mno",
	"7": "pqrs",
	"8": "tuv",
	"9": "wxyz",
}

func letterCombinationsdp(digits string) []string {
	if len(digits) == 0 {
		return []string{}
	}
	var result [][]string

	lastDigits := string(digits[len(digits)-1])
	digitsArr := phoneMap[lastDigits]
	result = append(result, []string{})

	for i := 0; i < len(digitsArr); i++ {
		result[0] = append(result[0], string(digitsArr[i]))
	}
	for i := len(digits) - 2; i >= 0; i-- {
		lastDigits = string(digits[i])
		digitsArr = phoneMap[lastDigits]
		var temRes []string
		for j := 0; j < len(digitsArr); j++ {
			lastArr := result[len(result)-1]
			for k := 0; k < len(lastArr); k++ {
				temRes = append(temRes, string(digitsArr[j])+lastArr[k])
			}
		}
		result = append(result, temRes)
	}
	return result[len(result)-1]
}

var combinations []string

func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return []string{}
	}
	combinations = []string{}
	backtrack(digits, 0, "")
	return combinations
}

func backtrack(digits string, index int, combination string) {
	if index == len(digits) {
		combinations = append(combinations, combination)
	} else {
		digit := string(digits[index])
		letters := phoneMap[digit]
		lettersCount := len(letters)
		for i := 0; i < lettersCount; i++ {
			backtrack(digits, index+1, combination+string(letters[i]))
		}
	}
}
