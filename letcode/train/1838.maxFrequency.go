package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(maxFrequency([]int{1, 2, 4}, 5))
	fmt.Println(maxFrequency([]int{1, 4, 8, 13}, 5))
	fmt.Println(maxFrequency([]int{3, 6, 9}, 2))
}

func maxFrequency(nums []int, k int) int {
	sort.Ints(nums)
	start, sum := 0, 0
	ans := 1
	for i := 1; i < len(nums); i++ {
		sum += (nums[i] - nums[i-1]) * (i - start)
		for sum > k {
			sum -= nums[i] - nums[start]
			start++

		}
		if i-start+1 > ans {
			ans = i - start + 1
		}

	}
	return ans

}
