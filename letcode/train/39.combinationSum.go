package main

import (
	"fmt"
)

func main() {
	fmt.Println(combinationSum2([]int{10, 1, 2, 7, 6, 1, 5}, 8))
	//fmt.Println(combinationSum([]int{2,3,6,7},7))
	//fmt.Println(combinationSum([]int{2, 3, 5}, 8))
}

func combinationSum(candidates []int, target int) [][]int {
	var result []int
	var arr [][]int
	var dfs func(candidates, result []int, begin, l, target int)

	dfs = func(candidates, result []int, begin, l, target int) {
		if target == 0 {
			var tmp = make([]int, len(result))
			copy(tmp, result)
			arr = append(arr, tmp)
			return
		}
		if target < 0 {
			return
		}
		for i := begin; i < l; i++ {
			result = append(result, candidates[i])
			dfs(candidates, result, i, l, target-candidates[i])
			result = result[:len(result)-1]
		}

	}
	dfs(candidates, result, 0, len(candidates), target)
	//fmt.Println(arr)
	return arr
}

func combinationSum1(candidates []int, target int) [][]int {
	res := [][]int{}
	var dfs func(start int, temp []int, sum int)

	dfs = func(start int, temp []int, sum int) {
		if sum >= target {
			if sum == target {
				newTmp := make([]int, len(temp))
				copy(newTmp, temp)
				res = append(res, newTmp)
			}
			return
		}
		for i := start; i < len(candidates); i++ {
			temp = append(temp, candidates[i])
			dfs(i, temp, sum+candidates[i])
			temp = temp[:len(temp)-1]
		}
	}
	dfs(0, []int{}, 0)
	return res
}

func combinationSum2(candidates []int, target int) [][]int {
	var result []int
	var arr [][]int
	var dfs func(candidates, result []int, begin, l, target int)

	dfs = func(candidates, result []int, begin, l, target int) {
		if target == 0 {
			var tmp []int = make([]int, len(result))
			copy(tmp, result)
			arr = append(arr, tmp)
			return
		}
		if target < 0 {
			return
		}
		for i := begin; i < l; i++ {
			result = append(result, candidates[i])
			dfs(candidates, result, i+1, l, target-candidates[i])
			result = result[:len(result)-1]
		}

	}
	dfs(candidates, result, 0, len(candidates), target)
	//fmt.Println(arr)
	return arr
}
