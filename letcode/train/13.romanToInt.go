package main

import "fmt"

func main() {
	fmt.Println(romanToInt("III"))
	fmt.Println(romanToInt("IV"))
	fmt.Println(romanToInt("IIIV"))
	fmt.Println(romanToInt("LVIII"))
	fmt.Println(romanToInt("MCMXCIV"))

}

func romanToInt(s string) int {
	numberMap := map[string]int{
		"I": 1,
		"V": 5,
		"X": 10,
		"L": 50,
		"C": 100,
		"D": 500,
		"M": 1000,
	}
	num := numberMap[string(s[0])]
	preValue := num
	for i := 1; i < len(s); i++ {
		if v, hasKey := numberMap[string(s[i])]; hasKey {
			if preValue < v {
				//说明是一个左边,需要多减一次
				num = num + v - 2*preValue
			} else {
				num += v
			}
			preValue = v
		}
	}
	return num

}
