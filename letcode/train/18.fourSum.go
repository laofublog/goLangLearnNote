package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(fourSum([]int{-5, -4, -3, -2, 1, 3, 3, 5}, -11))
}

func fourSum(nums []int, target int) [][]int {
	sort.Ints(nums)
	var result [][]int
	n := len(nums)
	if len(nums) < 4 {
		return result
	}

	for i := 0; i < n-3; i++ {
		var arr []int
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}
		arr = append(arr, nums[i])
		for j := i + 1; j < n-2; j++ {
			if j > i+1 && nums[j] == nums[j-1] {
				continue
			}
			arr = append(arr, nums[j])
			for k := j + 1; k < n-1; k++ {
				if k > j+1 && nums[k] == nums[k-1] {
					continue
				}
				arr = append(arr, nums[k])
				for h := k + 1; h < n; h++ {
					if h > k+1 && nums[h] == nums[h-1] {
						continue
					}
					if target == nums[i]+nums[j]+nums[k]+nums[h] {
						arr = append(arr, nums[h])
						temp := make([]int, len(arr))
						copy(temp, arr)
						result = append(result, temp)
						arr = arr[0 : len(arr)-1]
					}
				}
				arr = arr[0 : len(arr)-1]
			}
			arr = arr[0 : len(arr)-1]
		}
		arr = arr[0 : len(arr)-1]
	}
	return result
}
