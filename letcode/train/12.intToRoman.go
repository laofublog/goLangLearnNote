package main

import "fmt"

func main() {
	//roman := intToRoman(1994)
	roman := intToRoman(11)
	fmt.Print(roman)
}

func intToRoman(num int) string {
	numberMap := map[int]string{
		1:    "I",
		4:    "IV",
		5:    "V",
		9:    "IX",
		10:   "X",
		40:   "XL",
		50:   "L",
		90:   "XC",
		100:  "C",
		400:  "CD",
		500:  "D",
		900:  "CM",
		1000: "M",
	}
	numArr := []int{
		1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1,
	}
	if value, hasKey := numberMap[num]; hasKey {
		return value
	}
	var result string
	var i int = 0
	for num > 0 {
		if num < numArr[i] {
			i++
			continue
		}
		num = num - numArr[i]
		result = result + numberMap[numArr[i]]
		if i >= len(numArr)-1 {
			i = len(numArr) - 1
		}

	}
	return result

}
