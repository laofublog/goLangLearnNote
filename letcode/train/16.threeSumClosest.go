package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	fmt.Println(threeSumClosest([]int{-1, 2, 1, -4}, 1))
	fmt.Println(threeSumClosest([]int{0, 0, 0}, 1))
	fmt.Println(threeSumClosest([]int{0, 1, 2}, 3))
	fmt.Println(threeSumClosest([]int{1, 1, 1, 1}, 0))
	fmt.Println(threeSumClosest([]int{1, 1, 1, 0}, -100))
	fmt.Println(threeSumClosest([]int{1, 1, 1, 0}, 100))
	fmt.Println(threeSumClosest([]int{1, 1, -1}, 2))
}

func threeSumClosest(nums []int, target int) int {
	if len(nums) < 3 {
		return 0
	}
	sort.Ints(nums)
	maxValue := nums[0] + nums[1] + nums[len(nums)-1]
	if len(nums) == 3 {
		return maxValue
	}
	for start := 0; start < len(nums)-2; start++ {
		//防止重复解
		if start > 0 && nums[start-1] == nums[start] {
			continue
		}
		end := len(nums) - 1
		mid := start + 1
		for mid != end {
			sum := nums[start] + nums[mid] + nums[end]
			if sum == target {
				return sum
			}
			if math.Abs(float64(sum-target)) < math.Abs(float64(maxValue-target)) {
				maxValue = sum
			}
			if sum > target {
				end--
			} else {
				mid++
			}

		}

	}
	return maxValue
}
