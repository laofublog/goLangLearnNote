package main

import "fmt"

func main() {

	//arr := []int{1, 8, 6, 2, 5, 4, 8, 3, 7}
	//arr := []int{1, 1}
	arr := []int{4,3,2,1,4}
	area := maxArea(arr)
	fmt.Print(area)
}
//双指针算法
//1. 指针分别从首尾开始计算面积
//2. 移动小的指针，知道全部计算完成
func maxArea(height []int) int {
	maxAreaValue, start, end := 0, 0, len(height)-1

	for start < end {
		minValue := 0
		if height[end] < height[start] {
			minValue = height[end]
			end--
		} else {
			minValue = height[start]
			start++
		}
		area := (end - start + 1) * minValue
		if maxAreaValue < area {
			maxAreaValue = area
		}

	}
	return maxAreaValue

}
