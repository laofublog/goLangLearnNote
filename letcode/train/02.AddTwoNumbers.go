package main

import "fmt"
import "../utils"

func main() {
	l1 := &utils.ListNode{
		Val: 2,
		Next: &utils.ListNode{
			Val: 4,
			Next: &utils.ListNode{
				Val:  3,
				Next: nil,
			},
		},
	}

	l2 := &utils.ListNode{
		Val: 5,
		Next: &utils.ListNode{
			Val: 6,
			Next: &utils.ListNode{
				Val:  4,
				Next: nil,
			},
		},
	}
	numbers := addTwoNumbers(l1, l2)
	for numbers != nil {
		fmt.Println(numbers.Val)
		numbers = numbers.Next
	}
}

func addTwoNumbers(l1 *utils.ListNode, l2 *utils.ListNode) (head *utils.ListNode) {
	carry := 0
	var tail *utils.ListNode
	for l1 != nil || l2 != nil {
		n1, n2 := 0, 0
		if l1 != nil {
			n1 = l1.Val
			l1 = l1.Next
		}
		if l2 != nil {
			n2 = l2.Val
			l2 = l2.Next
		}
		sum := n1 + n2 + carry
		sum, carry = sum%10, sum/10
		if head == nil {
			head = &utils.ListNode{
				Val: sum,
			}
			tail = head
		} else {
			tail.Next = &utils.ListNode{
				Val: sum,
			}
			tail = tail.Next
		}
	}
	if carry > 10 {
		tail.Next = &utils.ListNode{
			Val: carry,
		}

	}
	return head

}
