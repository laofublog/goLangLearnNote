package main

import (
	"fmt"
)

func main() {
	fmt.Println(divide(25, 1))
}

func divide(dividend int, divisor int) int {
	val := 0
	valRange := 1
	flag := dividend > 0 && divisor > 0 || dividend < 0 && divisor < 0
	if dividend < 0 {
		dividend = (-1) * dividend
	}
	if divisor < 0 {
		divisor = (-1) * divisor
	}

	for dividend > 0 {
		for dividend-divisor >= divisor {
			divisor = divisor + divisor
			fmt.Println("divisor:", divisor)
			valRange++
		}
		dividend = dividend - divisor
	}

	fmt.Println(valRange)
	//if val > math.MaxInt32 {
	//	val = math.MaxInt32
	//}
	if flag {
		return val
	}

	return val * (-1)

}
