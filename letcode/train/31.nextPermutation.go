package main

import (
	"fmt"
	"sort"
)

func main() {
	arr := []int{5, 4, 7, 5, 3, 2}
	//arr := []int{5,1,1}
	//arr := []int{3, 2, 1}
	nextPermutation(arr)
	fmt.Println(arr)
}

func nextPermutation(nums []int) {

	cursor := len(nums) - 1
	if cursor == 0 {
		return
	}
	for nums[cursor-1] >= nums[cursor] {
		cursor--
		if cursor == 0 {
			sort.Ints(nums)
			return
		}
	}

	for k := len(nums) - 1; k > 0; k-- {
		if nums[k] > nums[cursor-1] {
			nums[cursor-1], nums[k] = nums[k], nums[cursor-1]
			break
		}
	}
	for j := cursor; j < len(nums)-cursor; j++ {
		nums[j], nums[len(nums)-j+cursor-1] = nums[len(nums)-j+cursor-1], nums[j]
	}
	return
}
