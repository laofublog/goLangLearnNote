package main

import (
	"fmt"
)

func main() {
	fmt.Println(minDays(56))

}
func minDays(n int) int {
	m := make(map[int]int)
	return core(n,m)
}

func core(n int, m map[int]int) int {
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	ret, hasKey := m[n]
	if hasKey {
		return ret
	}
	ret = min(core(n/2,m)+n%2+1, min(core(n/3,m)+n%3+1, n))

	m[n] = ret
	return ret
}

func min(v1, v2 int) int {
	if v1 > v2 {
		return v2
	}
	return v1
}
