package main

import "fmt"

func main() {
	fmt.Println( threeConsecutiveOdds([]int{2,6,4,1}))
	fmt.Println( threeConsecutiveOdds([]int{1, 2, 34, 3, 4, 5, 7, 23, 12}))

}


func threeConsecutiveOdds(arr []int) bool {
	if len(arr)<3{
		return false
	}
	count:=0
	for i := 0; i < len(arr); i++ {
		if arr[i]%2==1{
			count++
		}else {
			count=0
		}
		if count==3{
			return true
		}
	}
	return false

}