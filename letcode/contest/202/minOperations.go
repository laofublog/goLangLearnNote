package main

import (
	"fmt"
)

func main() {
	fmt.Println(minOperations(6))
}

func minOperations(n int) int {

	mid := n / 2
	midValue := 2*mid + 1
	if n%2 == 0 {
		midValue = midValue - 1
	}
	sum := 0
	for i := 0; i < mid; i++ {
		sum += (midValue - 2*i - 1)
	}
	return sum

}
