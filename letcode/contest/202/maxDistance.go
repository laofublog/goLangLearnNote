package main

import (
	"fmt"
	"sort"
)

func main() {
	position := []int{5, 4, 3, 2, 1, 1000000000}
	fmt.Println(maxDistance(position, 2))
}

/**
step 1.
	首先我们对position排序，得到按顺序排列的篮子

step 2.
	最小的磁力：就是这些m个篮子间那个最短的距离
	最大的磁力：就是把最大间距（position的尾减头）平均分，保证篮子间的距离都一样
	所以能取到的磁力范围必定落在【最小磁力，最大磁力】这个区间上

step 3.
	可以从小往大找，也可以从大往小找，也可以使用二分法从中间开始找，这种时间复杂度应该是最低的
	所谓的磁力其实就是间隔，假设我们区间中间的磁力是mid，要对间隔 mid 的放球法做出判断，看看能不能这样放
	验证的方法是：从第一个篮子开始逐个放球，如果下一个篮子距离上一个篮子没有mid那么远，就选后面的一个篮子，直至放球的篮子间达到距离mid的要求。如果球全放完了，证明成功；如果有球无法按要求放了则证明失败

step 4.
	如果step 3中的验证成功，我们可以试试更大的距离；如果失败，则用更小的距离。更大更小的距离也是通过二分法选择，直至找到那个最大的距离
 **/
func maxDistance(position []int, m int) int {
	sort.Ints(position)
	//1. 获得最大的距离
	hi := (position[len(position)-1] - position[0]) / (m - 1)
	//2. 最小的距离是 1
	lo := 1
	ans := 1
	//3. 距离一定在 最大和最小之间 ，使用二分法寻找
	for lo <= hi {
		mid := lo + (hi-lo)/2
		//4. 判断找到的位置 是否能够把剩余的球放下
		//5. 如果从前半段能够把球放下，则继续把 mid 向后移动，尝试更大的范围
		//6. 同理如果前半段放不下，则需要压缩 mid 的值
		if judge(position, mid, m) {
			ans = mid
			lo = mid + 1
		} else {
			hi = mid - 1
		}
	}
	return ans
}

func judge(position []int, mid, m int) bool {
	count := 1
	i := 0
	//判断在 position 中，放 m 个求能否放下。
	for j := 1; j < len(position); j++ {
		if position[j]-position[i] >= mid {
			i = j
			count++
			if count >= m {
				return true
			}
		}
	}
	return false
}
