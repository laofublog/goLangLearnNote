package main

import "fmt"

func main() {
	arr := []int{1, 3, 5}
	subarrays := numOfSubarrays(arr)
	fmt.Println(subarrays)
}
func numOfSubarrays(arr []int) int {
	ans, old := 0, 0
	for i := 0; i < len(arr); i++ {
		if arr[i]&1 == 1 {
			old += i + 1 - old
		}
		ans += old

	}
	return ans
}
