package main

import (
	"fmt"
)

func main() {
	arr := []int{3, 4, 2, 5, 6}
	operations := minNumberOperations(arr)
	fmt.Println(operations)
}

func minNumberOperations(target []int) int {
	ans := target[0]
	for i := 1; i < len(target); i++ {
		ans += max(target[i]-target[i-1], 0)
	}
	return ans
}
func max(s1, s2 int) int {
	if s1 > s2 {
		return s1
	}
	return s2
}
