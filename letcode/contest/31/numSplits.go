package main

import "fmt"

func main() {
	splits := numSplits("aaaaa")
	fmt.Println(splits)
}

func numSplits(s string) int {
	if s == "" {
		return 1
	}
	m := make(map[byte]int)
	removeMap := make(map[byte]int)

	time := 0
	for i := 0; i < len(s); i++ {
		m[s[i]]++
	}
	for i :=0; i < len(s); i++ {
		removeMap[s[i]]++
		m[s[i]]--
		if m[s[i]] == 0 {
			delete(m, s[i])
		}

		if len(removeMap) == len(m) {
			time++
		}
	}
	return time

}
