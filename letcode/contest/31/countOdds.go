package main

import "fmt"

func main() {
	odds := countOdds(3, 7)
	fmt.Println(odds)
}

func countOdds(low int, high int) int {
	if low%2 == 0 {
		low = low + 1
	}
	if high%2 == 0 {
		high = high - 1
	}

	return (high - low) / 2+1
}
