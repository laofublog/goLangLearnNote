package main

import "fmt"

func main() {
	arr := []int{1,2,3,4,5,6,7}
	subarrays := numOfSubarrays(arr)
	fmt.Println(subarrays)
}
func numOfSubarrays1(arr []int) int {
	var oddArr []int
	var evArr []int
	for i := 0; i < len(arr); i++ {
		if arr[i]%2 == 1 {
			oddArr = append(oddArr, arr[i])
		} else {
			evArr = append(evArr, arr[i])
		}
	}
	oddLen := len(oddArr)
	if oddLen == 0 {
		return 0
	}
	evenLen := len(evArr)
	evCount := (evenLen + 1) * evenLen / 2
	if oddLen == 1 {
		return evenLen
	}
	oddCount := oddLen
	for i := 3; i <= oddLen; i += 2 {
		oddCount += oddLen - i + 1
	}
	if evenLen == 0 {
		return oddCount
	}
	return oddCount * evCount

}
