package main

import (
	"fmt"
	"math"
)

func main() {
	s := "111111"
	sub := numSub(s)
	fmt.Println(sub)

}

func numSub(s string) int {
	var dp []int
	n := len(s)
	l := 0
	if s[0] == '1' {
		dp = append(dp, 1)
		l = 1
	} else {
		dp = append(dp, 0)
	}
	for i := 1; i < n; i++ {
		if s[i] == '1' {
			l++
			dp = append(dp, dp[i-1]+l)

		} else {
			dp = append(dp, dp[i-1])
			l = 0
		}

	}
	mod := int((math.Pow(float64(10), float64(9))) + 7)
	return dp[n-1] % mod
}
