package main

import "fmt"

func main() {
	n := 5
	edges := [][]int{{2, 3}, {1, 2}, {3, 4}, {1, 3}, {1, 4}, {0, 1}, {2, 4}, {0, 4}, {0, 2}}
	sucProb := []float64{0.06, 0.26, 0.49, 0.25, 0.2, 0.64, 0.23, 0.21, 0.77}
	start := 0
	end := 3
	probability := maxProbability(n, edges, sucProb, start, end)
	fmt.Println(probability)
}

func maxProbability(n int, edges [][]int, succProb []float64, start int, end int) float64 {
	var maxProb float64 = 1.0
	var director float64 = 0.0
	fmt.Printf("start:%v,end:%v", start, end)
	fmt.Println()
	for i := 0; i < len(edges); i++ {
		if edges[i][1] == end {
			if edges[i][0] == 0 {
				director = succProb[i]
			} else {
				nEnd:=edges[i][0]
				maxProb = maxProb * succProb[i]
				nSuccPro := succProb[0:i]
				nSuccPro = append(nSuccPro, succProb[i+1:]...)
				nEdage := edges[0:i]
				nEdage = append(nEdage, edges[i+1:]...)
				maxProb = maxProb * maxProbability(n-1, nEdage, nSuccPro, start, nEnd)
			}
		}
	}
	if director == 0.0 && maxProb == 1.0 {
		return 0
	}
	if maxProb != 1.0 && maxProb > director {
		return maxProb
	}
	return director
}
