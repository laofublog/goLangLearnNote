package main

import (
	"fmt"
	"sort"
)

func main() {
	arr := []int{1, 2, 3}
	pairs := numIdenticalPairs(arr)
	fmt.Println(pairs)
}

func numIdenticalPairs(nums []int) int {
	count := 0
	sort.Ints(nums)
	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				count++
			}
		}
	}
	return count
}
