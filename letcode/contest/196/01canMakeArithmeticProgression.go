package main

import "fmt"

func main() {
	arr := []int{1, 2, 4}
	progression := canMakeArithmeticProgression(arr)
	fmt.Println(progression)
}

func canMakeArithmeticProgression(arr []int) bool {

	quickSort(arr, 0, len(arr)-1)
	reduce := arr[1] - arr[0]
	for i := 1; i < len(arr)-1; i++ {
		if arr[i+1]-arr[i] != reduce {
			return false
		}
	}
	return true
}

func quickSort(arr []int, start, end int) {
	if start < end {
		i, j := start, end
		key := arr[(start+end)/2]
		for i <= j {
			for arr[i] < key {
				i++
			}
			for arr[j] > key {
				j--
			}
			if i <= j {
				arr[i], arr[j] = arr[j], arr[i]
				i++
				j--
			}
		}

		if start < j {
			quickSort(arr, start, j)
		}
		if end > i {
			quickSort(arr, i, end)
		}
	}
}
