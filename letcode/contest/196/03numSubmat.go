package main

import (
	"fmt"
)

func main() {
	arr := [][]int{
		{1, 1, 1, 1, 0},
		{1, 0, 0, 1, 0},
		{0, 0, 1, 0, 1},
		{0, 1, 0, 0, 0},
	}
	submat := numSubmat(arr)
	fmt.Println(submat)
}
func numSubmat(mat [][]int) int {
	dp := [][]int{}
	rowCol := len(mat)
	colCount := len(mat[0])
	dp[0][0] = 1
	for i := 1; i < rowCol; i++ {
		dp[i][0] = dp[i-1][0] + 1
		for j := 1; j < colCount; j++ {
			dp[i][j] = dp[i][j-1] + 1
		}

	}
	fmt.Println(dp)

	for i := 0; i < rowCol; i++ {
		for j := 1; j < colCount; j++ {
			if mat[i][j] == 1 {
				dp[i][j] = dp[i][j-1] + 1
			}

		}

	}

	sum := 0
	for i := 0; i < colCount; i++ {
		for j := 1; j < rowCol; j++ {
			if mat[j][i] == 1 {
				if i == 0 {
					dp[j][i] = dp[j-1][i] + 1
				} else {
					dp[j][i] = dp[j][i] + 1
				}
			}
			sum += dp[j][i]
		}

	}
	fmt.Println(dp)

	return sum

}
