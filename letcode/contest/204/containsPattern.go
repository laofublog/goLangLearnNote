package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(containsPattern([]int{1, 2, 2, 2, 1, 1, 2, 2, 2}, 1, 3))
	fmt.Println(containsPattern([]int{1, 2, 1, 2, 1, 1, 1, 3}, 2, 2))
	fmt.Println(containsPattern([]int{1, 2, 1, 2, 1, 3}, 2, 3))
	fmt.Println(containsPattern([]int{1, 2, 3, 1, 2}, 2, 2))
	fmt.Println(containsPattern([]int{2, 2, 2, 2}, 2, 3))
	//[3,2,2,1,2,2,1,1,1,2,3,2,2]
	//3
	//2
	fmt.Println(containsPattern([]int{3, 2, 2, 1, 2, 2, 1, 1, 1, 2, 3, 2, 2}, 3, 2))
}

func containsPattern(arr []int, m int, k int) bool {
	var count int
	for i := 0; i < len(arr)-m+1; i++ {
		keyArr := arr[i : i+m]
		str := getStr(keyArr)
		for j := i + m; j < len(arr); j += m {
			keyArr = arr[j : j+m]
			s := getStr(keyArr)
			if str == s {
				count++
			}
		}
		if count >= k {
			return true
		}
	}
	return false
}

func getStr(arr []int) string {
	var str string
	for i := 0; i < len(arr); i++ {
		str += strconv.Itoa(arr[i])
	}
	return str
}
