package main

import "fmt"

func main() {
	flips := minFlips("001011101")
	fmt.Println(flips)
}

func minFlips(target string) int {
	count := 0
	byteFlag := uint8('0')
	for i := 0; i < len(target); i++ {
		if target[i] != byteFlag {
			if byteFlag == '0' {
				byteFlag = uint8('1')
			} else {
				byteFlag = uint8('0')
			}

			count++
		}
	}
	return count

}

//func convert(initArr []byte, start int) {
//	for i := start; i < len(initArr); i++ {
//		if initArr[i]=='0'{
//			initArr[i]='1'
//		}else {
//			initArr[i]='0'
//		}
//
//	}
//}
