package main

import "fmt"

func main() {
	arr := []int{4,5,6,7,0,2,1,3}
	s := restoreString("codeleet", arr)
	fmt.Println(s)
}

func restoreString(s string, indices []int) string {
	var strArr []string
	for i := 0; i < len(s); i++ {
		strArr = append(strArr, string(s[i]))
	}
	for i := 0; i < len(indices); i++ {
		strArr[indices[i]] = string(s[i])
	}
	var str string
	for i := 0; i < len(strArr); i++ {
		str += strArr[i]
	}
	return str

}
