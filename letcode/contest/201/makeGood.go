package main

import "fmt"

func main() {
	 fmt.Println(makeGood("leEeetcode"))
	fmt.Println(makeGood("abBAcC"))
	fmt.Println(makeGood("s"))
	fmt.Println(makeGood("sS"))
	fmt.Println(makeGood("Pp"))
}
func makeGood(s string) string {
	if len(s) == 1 {
		return s
	}

	for i := 0; i < len(s)-1; {
		if s[i] == s[i+1]+32 || s[i] == s[i+1]-32 {
			s = s[:i] + s[i+2:]
			i = 0
		} else {
			i++
		}
	}

	return s

}
