package main

import (
	"fmt"
	"strings"
)

func main() {

	fmt.Println(string(findKthBit(3,1)))
	fmt.Println(string(findKthBit(4,11)))
	fmt.Println(string(findKthBit(1,1)))
	fmt.Println(string(findKthBit(2,3)))

}
func findKthBit(n int, k int) byte {
	arr := make([]string, n)
	arr[0] = "0"
	for i := 1; i < n; i++ {
		arr[i] = arr[i-1] + "1" + reverseAndInvert(arr[i-1])
	}
	return arr[n-1][k-1]

}

func reverseAndInvert(arr string) string {
	var result strings.Builder
	for i := len(arr) - 1; i >=0; i-- {
		if arr[i] == '1' {
			result.WriteByte('0')
		} else {
			result.WriteByte('1')
		}

	}

	return result.String()
}
