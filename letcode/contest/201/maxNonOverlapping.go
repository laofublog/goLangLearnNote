package main

import (
	"fmt"
)

func main() {
	//fmt.Println(maxNonOverlapping([]int{1, 1, 1, 1, 1}, 2))
	fmt.Println(maxNonOverlapping([]int{-1, 3, 5, 1, 4, 2, -9}, 6))
	fmt.Println(maxNonOverlapping([]int{-2, 6, 6, 3, 5, 4, 1, 2, 8}, 10))
	fmt.Println(maxNonOverlapping([]int{0, 0, 0}, 0))
	//[2,2,3,3,-3,-1,2,-3]
	//4
	fmt.Println(maxNonOverlapping([]int{-5, 5, -4, 5, 4}, 5))
	fmt.Println(maxNonOverlapping([]int{2, 2, 3, 3, -3, -1, 2, -3}, 4))
}

func maxNonOverlapping(nums []int, target int) int {
	count := 0
	sum := 0
	m := make(map[int]int)
	m[0] = 1
	for i := 0; i < len(nums); i++ {
		sum += nums[i]
		_, hasKey := m[sum-target]
		if hasKey {
			m = make(map[int]int)
			count++
			sum = 0
		}
		m[sum]++ //无用
	}

	return count

}
func subarraySum(nums []int, k int) int {
	count := 0
	sum := 0
	m := make(map[int]int)
	m[0] = 1
	for i := 0; i < len(nums); i++ {
		sum += nums[i]
		_, hasKey := m[sum-k]
		if hasKey {
			count++
			sum = 0
		}
		m[sum]++ //无用
	}

	return count
}