package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	//输入：n = 7, cuts = [1,3,4,5]
	n := 7
	cuts := []int{1, 3, 4, 5}

	fmt.Println(minCost(n, cuts))

}

func minCost(n int, cuts []int) int {
	m := len(cuts)
	sort.Ints(cuts)
	var arr []int
	arr = append(arr, 0)
	arr = append(arr, cuts...)
	arr = append(arr, n)

	dp := make([][]int, m+3)
	for i := 0; i <= m+2; i++ {
		dp[i] = make([]int, m+3)
	}

	for i := m; i >= 1; i-- {
		for j := i; j <= m; j++ {
			if i == j {
				dp[i][j] = 0
			} else {
				dp[i][j] = math.MaxInt64
			}
			for k := i; k <= j; k++ {
				dp[i][j] = min(dp[i][j], dp[i][k-1]+dp[k+1][j])
			}
			dp[i][j] += arr[j+1] - arr[i-1]
		}
	}
	return dp[1][m]
	//return 0
}

func min(v1, v2 int) int {
	if v1 > v2 {
		return v2
	}
	return v1
}
