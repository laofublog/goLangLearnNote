package main

import (
	"fmt"
)

func main() {
	fmt.Println('a')
	fmt.Println('z')
	fmt.Println(modifyString("?zs"))
	fmt.Println(modifyString("ubv?w"))
	fmt.Println(modifyString("j?qg??b"))
	fmt.Println(modifyString("??yw?ipkj?"))
	fmt.Println(modifyString("????????????????"))
	fmt.Println(modifyString("?"))

}
func modifyString(s string) string {
	if s == "?" {
		return "a"
	}
	result := []byte(s)
	for i := 0; i < len(result); i++ {
		if result[i] != '?' {
			continue
		}
		if i == 0 {
			if result[i+1] == '?' {
				result[i] = 'a'
				continue
			}
			result[i] = result[i+1] + 1
		} else if i == len(s)-1 {
			result[i] = result[i-1] + 1
		} else {
			if result[i+1] > result[i-1] {
				result[i] = result[i+1] + 1
			} else {
				result[i] = result[i-1] + 1
			}

		}
		if result[i] > 'z' {
			result[i] = result[i] - 'z' + 'a' - 1
		}
		if result[i] < 'a' {
			result[i] = 'z' - ('a' - result[i])
		}

	}
	return string(result)
}
