package main

import "fmt"

func main() {
//[14,1,1,12,7,12,10,4,11,10,5,2,5,14,7,9,10,13,15,6,9,12,6,12,4,10,9,12,11]
	//[3,12,1,9,1,12,4,12,4,1,7,10,7,11,4,13,4,11,5,1,14,12,15,4,2,3,13,10,3,4]
	fmt.Println(numTriplets([]int{4}, []int{2, 8}))
	fmt.Println(numTriplets([]int{1, 1}, []int{1, 1, 1}))
	fmt.Println(numTriplets([]int{7, 7, 8, 3}, []int{1, 2, 9, 7}))
	fmt.Println(numTriplets([]int{14,1,1,12,7,12,10,4,11,10,5,2,5,14,7,9,10,13,15,6,9,12,6,12,4,10,9,12,11}, []int{3,12,1,9,1,12,4,12,4,1,7,10,7,11,4,13,4,11,5,1,14,12,15,4,2,3,13,10,3,4}))

}
func numTriplets(nums1 []int, nums2 []int) int {
	count := 0

	count += getCount(nums1, nums2)
	count += getCount(nums2, nums1)

	return count/2
}
func getCount(nums1 []int, nums2 []int) int {
	m1 := make(map[int]int)
	m2 := make(map[int]int)
	count := 0
	for i := 0; i < len(nums1); i++ {
		m1[nums1[i]]++
	}
	for i := 0; i < len(nums2); i++ {
		m2[nums2[i]]++
	}
	for i := 0; i < len(nums1); i++ {
		for j := 0; j < len(nums2); j++ {
			if nums1[i]*nums1[i]%nums2[j] == 0 {
				i2 := nums1[i] * nums1[i] / nums2[j]
				count += m2[i2]
				if i2 == nums2[j] {
					count--
				}
			}
		}
	}
	return count
}
