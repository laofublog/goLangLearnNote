package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(maxCoins([]int{2, 4, 1, 2, 7, 8}))
	fmt.Println(maxCoins([]int{2, 4, 5}))
	fmt.Println(maxCoins([]int{9, 8, 7, 6, 5, 1, 2, 3, 4}))

}
func maxCoins(piles []int) int {
	if len(piles)==0{
		return 0
	}
	sort.Ints(piles)
	maxCount := 0
	start, end := 0, len(piles)-1
	for start < end {
		start++
		maxCount += piles[end-1]
		end -= 2
	}
	return maxCount

}
