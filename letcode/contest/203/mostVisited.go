package main

import "fmt"

func main() {
	fmt.Println(mostVisited(4, []int{1,3,1,2}))
	fmt.Println(mostVisited(2, []int{2,1,2,1,2,1,2,1,2}))
	fmt.Println(mostVisited(7, []int{1, 3, 5, 7}))
}

func mostVisited(n int, rounds []int) []int {
	start, end := 0, 0
	m := make([]int, n)
	for i := 0; i < n; i++ {
		m[i] = 0
	}
	m[rounds[0]-1] = 1
	for i := 1; i < len(rounds); i++ {
		start = rounds[i-1]
		end = rounds[i]
		if end < start {
			end += n
		}
		for j := start + 1; j <= end; j++ {
			if j > n {
				m[j-n-1]++
			} else {
				m[j-1]++
			}
		}
	}
	var result []int
	max := 0
	for i := 0; i < len(m); i++ {
		if m[i] > max {
			result = []int{}
			max=m[i]
		}
		if m[i] == max {
			result = append(result, i+1)
		}
	}

	return result

}
