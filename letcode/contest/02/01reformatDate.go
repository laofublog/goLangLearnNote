package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	date :="6th Jun 1933"
	s := reformatDate(date)
	fmt.Println(s)
}

func reformatDate(date string) string {
	dateArr := strings.Split(date, " ")
	if len(dateArr)<3{
		return ""
	}
	year := dateArr[2]
	month := getMonth(dateArr[1])

	day := dateArr[0]
	day=day[:len(day)-2]
	if len(day)<2{
		day="0"+day
	}
	return year + "-" +month + "-" + day

}

func getMonth(month string) string {
	monthArr := []string{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
	for i := 0; i < len(monthArr); i++ {
		if month == monthArr[i] {
			if (i+1)<10{
				return "0"+strconv.Itoa(i+1)
			}
			return strconv.Itoa(i+1)
		}
	}
	return "0"

}
