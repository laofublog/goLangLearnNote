package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	//arr := []int{6, 6, 0, 1, 1, 4, 6}
	arr := []int{1, 5, 6, 14, 15}
	difference := minDifference(arr)
	fmt.Println(difference)

}
func minDifference(nums []int) int {
	sort.Ints(nums)
	if len(nums) < 5 {
		return 0
	}
	maxVal := math.MaxInt64
	for i := 0; i < 4; i++ {
		tmp := nums[i : len(nums)-3+i]
		if maxVal > tmp[len(tmp)-1]-tmp[0] {
			maxVal = tmp[len(tmp)-1] - tmp[0]
		}

	}
	return maxVal
}

func getMaxVal(nums []int, time int) int {
	if time == 3 {
		return nums[len(nums)-1] - nums[0]
	}
	time++
	minVal := getMaxVal(nums[1:], time)
	maxVal := getMaxVal(nums[0:len(nums)-2], time)
	if minVal < maxVal {
		return minVal
	}
	return maxVal

}
