package main

import (
	"fmt"
	"sort"
)

func main() {
	arr := []int{1, 2, 3, 4}
	sum := rangeSum(arr, 4, 3, 4)
	fmt.Println(sum)
}

func rangeSum(nums []int, n int, left int, right int) int {
	var result []int
	for i := 0; i < n; i++ {
		var temp []int
		temp = append(temp, nums[i])
		for j := i + 1; j < n; j++ {
			temp = append(temp, temp[j-i-1]+nums[j])
		}

		result = append(result, temp...)
	}
	sort.Ints(result)
	sum:=0
	for i := left-1; i < right; i++ {
		sum+=result[i]
	}
	return sum

}
