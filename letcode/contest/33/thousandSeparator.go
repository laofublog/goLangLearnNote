package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(thousandSeparator(1234))
}
func thousandSeparator(n int) string {
	if n < 1000 {
		return strconv.Itoa(n)
	}
	x := strconv.Itoa(n % 1000)
	for len(x) < 3 {
		x = "0" + x
	}
	str := x
	n = n / 1000
	for n >= 1000 {
		x = strconv.Itoa(n % 1000)
		for len(x) < 3 {
			x = "0" + x
		}
		str = x + "." + str
		n = n / 1000
	}
	str = strconv.Itoa(n) + "." + str
	return str

}
