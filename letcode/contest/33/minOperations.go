package main

import "fmt"

func main() {
	fmt.Println(minOperations([]int{1, 5}))
	fmt.Println(minOperations([]int{2, 2}))
	fmt.Println(minOperations([]int{4, 2, 5}))
	fmt.Println(minOperations([]int{3,2,2,4}))
	fmt.Println(minOperations([]int{2,4,8,16}))
}

func minOperations(nums []int) int {
	count := 0
	mc := 0
	for true {
		for i := 0; i < len(nums); i++ {
			if nums[i] == 0 {
				mc++
				continue
			}
			if nums[i] == 1 {
				count++
				mc++
				nums[i]--
				continue
			}
			if nums[i]%2 != 0 {
				nums[i]--
				count++
			}

		}
		if mc == len(nums) {
			break
		}
		for i := 0; i < len(nums); i++ {
			nums[i] = nums[i] / 2
		}
		count++


		mc = 0
	}

	return count
}

func toZero(value int) int {
	count := 0
	for value > 0 {
		if value%2 == 0 {
			value = value / 2
		} else {
			value--
		}
		count++
	}
	return count
}
