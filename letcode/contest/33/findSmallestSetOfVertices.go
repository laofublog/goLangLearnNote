package main

import "fmt"

func main() {
	n := 5
	edges := [][]int{{0,1},{2,1},{3,1},{1,4},{2,4}}
	fmt.Println(findSmallestSetOfVertices(n, edges))
}
func findSmallestSetOfVertices(n int, edges [][]int) []int {
	result := []int{}
	degree := make(map[int]int)

	for i := 0; i < len(edges); i++ {
		degree[edges[i][1]]++
	}
	for k, v := range degree {
		if v == 0 {
			result = append(result, k)
		}
	}

	return result

}
