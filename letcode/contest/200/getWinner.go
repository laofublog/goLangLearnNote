package main

import (
	"fmt"
)

func main() {
	arr := []int{1,11,22,33,44,55,66,77,88,99}
	k := 1000000
	winner := getWinner(arr, k)
	fmt.Println(winner)
}

func getWinner(arr []int, k int) int {
	point:=arr[0]
	dp:=make([]int,len(arr))
	index:=0
	for i := 1; i < len(arr); i++ {
		if arr[i] > point {
			point = arr[i]
			index=i
			dp[index]++
		} else {
			dp[index]++
		}
		if dp[index] == k {
			break
		}
	}
	return arr[index]

}
