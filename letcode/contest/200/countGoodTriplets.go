package main

import (
	"fmt"
	"math"
)

func main() {
	arr := []int{1,1,2,2,3}
	a := 0
	b := 0
	c := 1
	triplets := countGoodTriplets(arr, a, b, c)
	fmt.Println(triplets)

}
func countGoodTriplets(arr []int, a int, b int, c int) int {
	count := 0
	arrLen := len(arr)
	for i := 0; i < arrLen; i++ {
		for j := i + 1; j < arrLen; j++ {
			for k := j + 1; k < arrLen; k++ {
				//|arr[i] - arr[j]| <= a
				//|arr[j] - arr[k]| <= b
				//|arr[i] - arr[k]| <= c
				if math.Abs(float64(arr[i]-arr[j])) <= float64(a) && math.Abs(float64(arr[j]-arr[k])) <= float64(b) && math.Abs(float64(arr[i]-arr[k])) <= float64(c) {
					count++
				}
			}
		}
	}
	return count

}
