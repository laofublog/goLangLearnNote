package main

import (
	"fmt"
	"math"
)

func main() {

	//[10,15,20,26,35,47,52,61,68,70,73,85,96,100,103,108,116,125,134,143,153,155]
	//[6,15,25,36,48,60,71,72,73,83,92,104,113,123,132,141,150,158,169,180,190,199,201,210,219,226,236,245,255,263]
	nums1 := []int{2, 4, 5, 8, 10}
	nums2 := []int{4, 6, 8, 9}
	sum := maxSum(nums1, nums2)
	fmt.Println(sum)
}

//i, j = len(nums1) - 1, len(nums2) - 1
//s1, s2 = 0, 0
//while i >= 0 and j >= 0:
//if nums1[i] > nums2[j]:
//s1 += nums1[i]
//i -= 1
//elif nums1[i] < nums2[j]:
//s2 += nums2[j]
//j -= 1
//else:
//s1 = s2 = max(s1, s2) + nums1[i]
//i -= 1
//j -= 1
//
//while i >= 0: s1 += nums1[i]; i -= 1;
//while j >= 0: s2 += nums2[j]; j -= 1;
//return max(s1, s2) % (10 ** 9 + 7)
//

func maxSum(nums1 []int, nums2 []int) int {
	l1 := len(nums1) - 1
	l2 := len(nums2) - 1
	s1, s2 := 0, 0
	for l1 >= 0 && l2 >= 0 {
		if nums1[l1] > nums2[l2] {
			s1 += nums1[l1]
			l1--
		} else if nums2[l2] > nums1[l1] {
			s2 += nums2[l2]
			l2--
		} else {
			s1 = maxValue(s1, s2) + nums1[l1]
			s2 = s1
			l1--
			l2--
		}
	}

	for l1 >= 0 {
		s1 += nums1[l1]
		l1--
	}
	for l2 >= 0 {
		s2 += nums2[l2]
		l2--
	}
	mod := int((math.Pow(float64(10), float64(9))) + 7)
	return maxValue(s1, s2)%mod
}

func maxValue(s1, s2 int) int {
	if s1 > s2 {
		return s1
	}
	return s2
}
