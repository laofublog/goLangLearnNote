package main

import "fmt"

func main() {
	bottles := numWaterBottles(15, 8)
	fmt.Println(bottles)
}

func numWaterBottles(numBottles int, numExchange int) int {
	sum := numBottles
	emptyCount := numBottles
	for emptyCount >= numExchange {
		mod:= emptyCount % numExchange
		emptyCount = emptyCount / numExchange
		sum += emptyCount
		emptyCount+=mod
	}
	return sum
}
