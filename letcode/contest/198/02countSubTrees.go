package main

import (
	"fmt"
)

//func main() {
//	edages := [][]int{
//		{0,1},{0,2},{1,4},{1,5},{2,3},{2,6},
//	}
//	labels := "abaedcd"
//	trees := countSubTrees(7, edages, labels)
//	fmt.Println(trees)
//}

func countSubTrees(n int, edges [][]int, labels string) []int {
	rowCnt := len(edges)
	var result []int
	var son []int
	for i := 0; i < n; i++ {
		result = append(result, 1)
	}

	for i := 0; i < rowCnt; i++ {
		son = append(son, edges[i][1])
		index1 := edges[i][0]
		if labels[index1] == labels[edges[i][1]] {
			result[index1]++
		}
		j := i + 1
		for len(son) > 0 && j < n-1 {
			index2 := edges[j][0]
			if labels[index1] == labels[index2] {
				result[index1]++
			}
			if inArr(son, index2) && !inArr(son, edges[j][1]) {
				son = append(son, edges[j][1])
			}
			son = son[1:]
			j++
		}
		son = []int{}

	}

	return result
}

func inArr(arr []int, value int) bool {
	for i := 0; i < len(arr); i++ {
		if arr[i] == value {
			return true
		}
	}
	return false

}

// mergerSort
func mergerSort(arr []int, start, end int) {
	if end-start <= 1 {
		return
	}

	mid := (start + end) / 2
	mergerSort(arr, start, mid)
	mergerSort(arr, mid, end)

	arrLeft := make([]int, mid-start)
	arrRight := make([]int, end-mid)
	copy(arrLeft, arr[start:mid])
	copy(arrRight, arr[mid:end])
	i := 0
	j := 0
	k := 0
	for i <= mid && j <= end {
		if arrLeft[i] < arrRight[j] {
			arr[k] = arrLeft[i]
			k++
			i++
		} else {
			arr[k] = arrLeft[j]
			k++
			j++
		}
	}
	for i <= mid {
		arr[k] = arrLeft[i]
		k++
		i++
	}
	for j <= end {
		arr[k] = arrLeft[j]
		k++
		j++

	}
}

func main() {
	// 测试代码
	arr := []int{9, 8, 7, 6, 5, 1, 2, 3, 4, 0}
	fmt.Println(arr)
	mergerSort(arr, 0, len(arr))
	fmt.Println(arr)
}
