package main

import (
	"fmt"
)

func preOrder(root *TreeNode) {
	if root == nil {
		return
	}
	fmt.Print(root.Val, " ")
	preOrder(root.Left)
	preOrder(root.Right)
}

func mediumOrder(root *TreeNode) {
	if root == nil {
		return
	}
	mediumOrder(root.Left)
	fmt.Print(root.Val, " ")
	mediumOrder(root.Right)
}

func mediumOrderCycle(root *TreeNode) {
	if root == nil {
		return
	}

	stack := &Stack{}
	for root != nil || stack.Len() > 0 {
		for root != nil {
			stack.push(root)
			root = root.Left
		}
		root = stack.pop()
		fmt.Print(root.Val, " ")
		root = root.Right

	}
}

func backOrder(root *TreeNode) {
	if root == nil {
		return
	}
	backOrder(root.Left)
	backOrder(root.Right)
	fmt.Print(root.Val, " ")

}

func searchBylevel(root *TreeNode) {
	var arr []*TreeNode
	arr = append(arr, root)
	var result [][]int
	for len(arr) > 0 {
		var stack []int
		var tempArr []*TreeNode
		for len(arr) > 0 {
			node := arr[0]
			arr = arr[1:]

			stack = append(stack, node.Val)
			if node.Left != nil {
				tempArr = append(tempArr, node.Left)
			}
			if node.Right != nil {
				tempArr = append(tempArr, node.Right)
			}
		}
		result = append(result, stack)
		arr = append(arr, tempArr...)
	}
	fmt.Println(result)
}
func BFS(root *TreeNode) []int {
	valueList := make([]int, 0)
	queue := make([]*TreeNode, 0)
	if root == nil{
		return valueList
	}

	queue = append(queue, root)
	for len(queue) > 0 {
		first := queue[0]
		valueList = append(valueList, first.Val)
		queue = queue[1:]

		if first.Left != nil {
			queue = append(queue, first.Left)
		}

		if first.Right != nil {
			queue = append(queue, first.Right)
		}
	}

	return valueList
}
func initTree(pre *[]int, mOrder *[]int, length int) *TreeNode {
	if length <= 0 {
		return nil
	}
	root := &TreeNode{Val: (*pre)[0]}
	//fmt.Println(root.Val)
	nRoot := 0
	ppre := *pre
	for root.Val != (*mOrder)[nRoot] {
		nRoot++
	}
	pre1 := ppre[1:]
	//左子树
	root.Left = initTree(&pre1, mOrder, nRoot)
	pre2 := ppre[nRoot+1:]
	mArr := (*mOrder)[nRoot+1:]
	//右子树
	root.Right = initTree(&pre2, &mArr, length-nRoot-1)
	return root
}
