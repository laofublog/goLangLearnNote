package main



/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func maxDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}
	leftDept := maxDepth(root.Left) + 1
	rigthDept := maxDepth(root.Right) + 1
	if leftDept > rigthDept {
		return leftDept
	}
	return rigthDept
}
