package main

//左左情况
func singalRotateLeft(node *TreeNode) {
	leftNode := node.Left
	node.Left = leftNode.Right
	leftNode.Right = node

}

func singalRotateRight(node *TreeNode) {
	rigthNode := node.Right
	node.Right = rigthNode.Left
	rigthNode.Left = node

}

func doubleRotateLR(node *TreeNode) {
	singalRotateRight(node)
	singalRotateLeft(node)

}

func doubleRotateRL(node *TreeNode) {

	singalRotateLeft(node)
	singalRotateRight(node)

}

func insertNodeAvl(node *TreeNode, val int) {
	if node == nil {
		node = &TreeNode{Val: val}
		return
	}
	if node.Val > val {
		insertNode(node.Left, val)
		if height(node.Left)-height(node.Right) == 2 {
			if val < node.Left.Val {
				singalRotateLeft(node)
			} else {
				doubleRotateLR(node)
			}
		}
	} else if node.Val < val {
		insertNode(node.Right, val)
		if height(node.Right)-height(node.Left) == 2 {
			if val > node.Right.Val {
				singalRotateRight(node)
			} else {
				doubleRotateRL(node)
			}
		}
	} else {
		node.freq++
	}
	node.height =maxHeight(height(node.Left),height(node.Right))
}

func height(node *TreeNode) int {
	return 0

}

func maxHeight(v1, v2 int) int {
	if v1 > v2 {
		return v1
	}
	return v2
}
