package main

import "math"

//
//func main() {
//	root := &TreeNode{Val: 2}
//	root.Left = &TreeNode{Val: 1}
//	root.Right = &TreeNode{Val: 3}
//	bst := isValidBST(root)
//	fmt.Println(bst)
//}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *TreeNode) bool {
	return helper(root, math.MinInt64, math.MaxInt64)
	//if root == nil {
	//	return true
	//}
	//rootValue := math.MinInt64
	//stack := &Stack{}
	//for root != nil || stack.Len() > 0 {
	//	for root != nil {
	//		stack.push(root)
	//		root = root.Left
	//	}
	//	root = stack.pop()
	//	if root.Val < rootValue {
	//		return false
	//	}
	//	rootValue = root.Val
	//	root = root.Right
	//
	//}
	//return true
}
//
//func isValidBSTDG(root *TreeNode) bool {
//	if root == nil {
//		return true
//	}
//	l := isValidBSTDG(root.Left)
//	if root.Val <= pre {
//		return false
//	}
//	pre = root.Val
//	r := isValidBSTDG(root.Right)
//	return l && r
//}
//
//func mediumOrderArr(root *TreeNode, arr *[]int) {
//	if root == nil {
//		return
//	}
//
//	mediumOrderArr(root.Left, arr)
//	p := append(*arr, root.Val)
//	arr = &p
//	mediumOrderArr(root.Right, arr)
//}

func helper(root *TreeNode, lower, upper int) bool {
	if root == nil {
		return true
	}
	if root.Val <= lower || root.Val >= upper {
		return false
	}
	return helper(root.Left, lower, root.Val) && helper(root.Right, root.Val, upper)
}
