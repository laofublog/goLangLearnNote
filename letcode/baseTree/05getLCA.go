package main

func getLac(root, node1, node2 *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
	//如果 node1 或者 node2 等于 root. 说明 一个一定是另一个祖先
	if node1 == root || node2 == root {
		return root
	}
	left := getLac(root.Left, node1, node2)
	rigth := getLac(root.Right, node1, node2)

	if left != nil && rigth != nil {
		return root

	} else if left != nil {
		return left
	} else if rigth != nil {
		return rigth
	} else {
		return nil
	}
}
