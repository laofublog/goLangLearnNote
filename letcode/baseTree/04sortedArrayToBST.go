package main

import "fmt"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func sortedArrayToBST(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	if len(nums) == 1 {
		return &TreeNode{Val: nums[0]}
	}

	medium := len(nums) / 2

	startArr := nums[:medium]
	endArr := nums[medium+1:]
	root := &TreeNode{Val: nums[medium]}
	root.Left = sortedArrayToBST(startArr)
	root.Right = sortedArrayToBST(endArr)

	return root
}

func firstBadVersion(n int) int {
	start := 0
	end := n
	i := (start + end) / 2
	for start < end {
		if isBadVersion(i) {
			end = i
		} else {
			start = i
		}
		fmt.Printf("start:%v end:%v", start, end)
		fmt.Println()
		i = (start + end) / 2
		if i == start || i == end {
			return i
		}
	}
	return i
}

func isBadVersion(i int) bool {
	//fmt.Print(0)
	return i > 4
}
