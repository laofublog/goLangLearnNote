package main

import "fmt"

func  InitSearchTree() *TreeNode{
	root := &TreeNode{Val: 10}
	root.Left = &TreeNode{Val: 5}
	root.Right = &TreeNode{Val: 15}
	root.Right.Left = &TreeNode{Val: 12}
	root.Right.Right = &TreeNode{Val: 16}
	root.Left.Left = &TreeNode{Val: 4}
	root.Left.Right = &TreeNode{Val: 6}
	root = insertNode2(root, 45)
	root = insertNode2(root, 13)
	isInTree := searchTree(root, 45)
	//deleteTreeNode(root, 15)
	fmt.Println(isInTree)
	return root
}
func searchTree(root *TreeNode, value int) bool {
	isSource := false
	for root != nil && !isSource {
		if root.Val == value {
			isSource = true
		}
		if root.Val < value {
			root = root.Right
			continue
		}
		if root.Val > value {
			root = root.Left
			continue
		}
	}
	return isSource
}

func searchTreeByNode(root *TreeNode, value int) *TreeNode {
	isSource := false
	for root != nil && !isSource {
		if root.Val == value {
			return root
		}
		if root.Val < value {
			root = root.Right
			continue
		}
		if root.Val > value {
			root = root.Left
			continue
		}
	}
	return nil
}
func insertNode(root *TreeNode, value int) *TreeNode {
	if root == nil {
		root = &TreeNode{Val: value}
		return root
	}
	if root.Val < value {
		root.Right = insertNode(root.Right, value)
	} else {
		root.Left = insertNode(root.Left, value)
	}
	return root
}

func insertNode2(root *TreeNode, value int) *TreeNode {
	if root == nil {
		root = &TreeNode{Val: value}
		return root
	}
	p := root
	pre := root
	for p != nil {
		pre = p
		if p.Val < value {
			p = p.Right
		} else {
			p = p.Left
		}
	}
	if pre.Val > value {
		pre.Left = &TreeNode{Val: value}
	}
	if pre.Val <= value {
		pre.Right = &TreeNode{Val: value}
	}

	return root
}

func deleteTreeNode(root *TreeNode, value int) {
	p := root
	pre := root
	for p != nil {
		if p.Val == value {
			break
		}
		pre = p
		if p.Val < value {
			p = p.Right
			continue
		}
		if p.Val > value {
			p = p.Left
			continue
		}
	}
	if p == nil {
		fmt.Println("未查询到该节点")
		return
	}

	//01. P为叶子节点
	if p.Left == nil && p.Right == nil {
		if pre.Right == p {
			pre.Right = nil
		}
		if pre.Left == p {
			pre.Left = nil
		}
		return
	}
	//02. P有叶子节点
	if p.Left == nil || p.Right == nil {
		if pre.Left == p && p.Right != nil {
			pre.Left = p.Right
		}
		if pre.Left == p && p.Left != nil {
			pre.Left = p.Left
		}
		if pre.Right == p && p.Right != nil {
			pre.Right = p.Right
		}
		if pre.Right == p && p.Left != nil {
			pre.Right = p.Left
		}
	}

	//38. P 有左节点 也有右节点
	if p.Left != nil && p.Right != nil {
		t := p
		s := p.Left
		//找到仅次于当前节点的值
		for s.Right != nil {
			t = s
			s = s.Right
		}
		p.Val = s.Val
		//t==p 如果 s 的右子树为空，直接把左子树移过来
		if t == p {
			p.Left = s.Left
		} else {
			//s 还有左子树，追加到父节点的右子树上
			t.Right = s.Left
		}

		return
	}

	return
}
