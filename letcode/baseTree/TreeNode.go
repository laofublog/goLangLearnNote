package main

type TreeNode struct {
	Val    int
	Left   *TreeNode
	Right  *TreeNode
	height int
	freq   int
}

type TreeNodeStr struct {
	Val   string
	Left  *TreeNode
	Right *TreeNode
}
