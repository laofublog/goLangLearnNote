package main

import (
	"strings"
)

func matchBrackes(str string) bool {
	left := "{[("
	right := "}])"
	var arr []string
	for i := 0; i < len(str); i++ {
		leftIndex := strings.Index(left, string(str[i]))
		if leftIndex > -1 {
			arr = append(arr, string(str[i]))
			continue
		}
		rightIndex := strings.Index(right, string(str[i]))
		if rightIndex > -1 {
			if len(arr) == 0 || string(arr[len(arr)-1]) != string(left[rightIndex]) {
				arr = arr[:len(arr)-1]
				return false
			}
			arr = arr[:len(arr)-1]
		}
	}
	return true
}

func getLongMatch(str string) int {
	var arr []int
	ans := 0
	for i := 0; i < len(str); i++ {
		if str[i] == '(' {
			arr = append(arr, i)
		} else {
			if len(arr) != 0 {
				if ans < i-arr[len(arr)-1]+1 {
					ans = i - arr[len(arr)-1] + 1
				}
				arr = arr[:len(arr)-1]
			}
		}
	}
	return ans

}

func getLongMatch01(s string) int {
	var arr []int
	ans := 0
	last := -1
	for i := 0; i < len(s); i++ {
		if s[i] == '(' {
			arr = append(arr, i)
		} else {
			//说明是第一个匹配
			if len(arr) == 0 {
				last = i
			} else {
				arr = arr[:len(arr)-1]
				//最后一个匹配
				if len(arr) == 0 {
					if ans < i-last {
						ans = i - last
					}
				} else {
					//匹配到最后，还剩余的'('
					if ans < i-arr[len(arr)-1] {
						ans = i - arr[len(arr)-1]
					}

				}
			}

		}
	}
	return ans

}
