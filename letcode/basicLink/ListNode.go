package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func print(head *ListNode) {
	fmt.Println()
	p := head
	for p != nil {
		fmt.Print(p.Val, "  ")
		p = p.Next
	}
}
