package main

//func main() {
//	head := &ListNode{Val: 1}
//	p := head
//	for i := 2; i < 4; i++ {
//		n := &ListNode{Val: i}
//		p.Next = n
//		p = p.Next
//	}
//	head1 := &ListNode{Val: 1}
//	p1 := head1
//	for i := 2; i < 4; i++ {
//		n1 := &ListNode{Val: i}
//		p1.Next = n1
//		p1 = p1.Next
//	}
//	print(head)
//	print(head1)
//	end := mergeTwoLists(head, head1)
//	print(end)
//}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	pre := &ListNode{Next: l1}
	p1 := pre
	p2 := l2

	for p2 != nil {
		if p1.Next == nil {
			break
		}
		if p1.Next.Val >= p2.Val {
			tmp := p2.Next
			p2.Next = p1.Next
			p1.Next = p2
			p2 = tmp

		} else {
			p1 = p1.Next
		}
	}
	if p2 != nil {
		p1.Next = p2

	}
	return pre.Next
}
