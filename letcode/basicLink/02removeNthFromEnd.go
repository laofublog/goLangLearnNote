package main

//func main() {
//	head := &ListNode{Val: 1}
//	p := head
//	for i := 2; i < 5; i++ {
//		n := &ListNode{Val: i}
//		p.Next = n
//		p = p.Next
//	}
//	print(head)
//	end := removeNthFromEnd(head, 2)
//	print(end)
//}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd(head *ListNode, n int) *ListNode {
	p1 := head
	pre := &ListNode{Next: head}
	count := 0
    p2:=pre
	for p1 != nil {
		p1 = p1.Next
		if count < n {
			count++
			continue
		}
		p2 = p2.Next
	}

	deleteNode := p2.Next
	p2.Next= deleteNode.Next
	deleteNode.Next = nil
	return pre.Next

}

