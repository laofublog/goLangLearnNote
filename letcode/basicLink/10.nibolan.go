package main

import (
	"../utils"
	"strconv"
	"strings"
)

func calcNbl(str string) int {
	operationalSymbol := "+-*/"
	stack := &utils.Stack{}
	value := 0
	for i := 0; i < len(str); i++ {
		if strings.Index(operationalSymbol, string(str[i])) > -1 {
			v1, _ := strconv.Atoi(stack.Pop().(string))
			v2, _ := strconv.Atoi(stack.Pop().(string))
			value = getValue(v1, v2, str[i])
			stack.Push(strconv.Itoa(value))
		} else {
			stack.Push(string(str[i]))
		}
	}
	return value
}

func getValue(v1, v2 int, opSymbol byte) int {
	switch opSymbol {
	case '+':
		return v1 + v2
	case '-':
		return v1 - v2
	case '*':
		return v1 * v2
	case '/':
		return v1 / v2
	default:
		panic("not support operation symbol")

	}
	return 0

}
