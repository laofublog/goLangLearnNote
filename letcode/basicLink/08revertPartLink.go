package main

func revertPartLink(head *ListNode, m, n int) {
	p := head
	pre := p
	for i := 0; i < m-1; i++ {
		pre = p
		p = p.Next
	}

	for i := m; i < n; i++ {
		q := p.Next
		p.Next = q.Next
		q.Next = pre.Next
		pre.Next = q

	}
}

func revertPartLinkSplit(head *ListNode, n int) {
	p := head
	for p != nil && p.Val != n {
		p = p.Next
	}
	if p == nil {
		return
	}

	for p.Next != nil {
		q := p.Next
		if q.Val < n {
			p.Next = q.Next
			q.Next = head.Next
			head.Next = q
		} else {
			p = p.Next
		}
	}
}

func deleteReapeteEl(root *ListNode) {
	pre := &ListNode{Next: root}
	p := root

	for p != nil {
		for p.Next != nil && p.Val == p.Next.Val {
			p = p.Next
			pre.Next = p.Next
			p = pre.Next
		}
		pre = pre.Next
		p = p.Next
	}
}
