package main

//
//func main() {
//	head := &ListNode{Val: 1}
//	p := head
//	arr := []int{-21, 10, 17, 8, 4, 26, 5, 35, 33, -7, -16, 27, -12, 6, 29, -12, 5, 9, 20, 14, 14, 2, 13, -24, 21, 23, -21, 5}
//	for i := 0; i < len(arr); i++ {
//		n := &ListNode{Val: arr[i]}
//		p.Next = n
//		p = p.Next
//	}
//	print(head)
//	fmt.Println(hasCycle(head))
//
//}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func hasCycle(head *ListNode) bool {
	fast := head.Next
	slow := head
	for fast != nil && fast.Next != nil {
		if fast == slow {
			return true
		}
		fast = fast.Next.Next
		slow = slow.Next

	}
	return false

}
