package main

import "../utils"

func maxZFT(arr []int) int {
	ans := 0
	area := &utils.Stack{}
	arr = append(arr, 0)
	//area.Push(0)
	for i := 0; i < len(arr); {
		if area.Len() == 0 || arr[i] > arr[area.Top().(int)] {
			area.Push(i)
			i++
		} else {
			index := area.Pop().(int)
			if area.Len() == 0 {
				tmpArea := arr[index] * i
				if ans < tmpArea {
					ans = tmpArea
				}
			} else {
				a := arr[index] * (i - area.Top().(int) - 1)
				if ans < a {
					ans = a
				}
			}

		}
	}
	return ans

}

func rainCollect(arr []int) int {
	trap := 0
	left := 0
	right := len(arr) - 1
	leftMax, rightMax := 0, 0
	for left < right {
		if arr[left] < arr[right] {
			if arr[left] > leftMax {
				leftMax = arr[left]
			} else {
				trap = trap + leftMax - arr[left]
			}
			left++
		} else {
			if arr[right] > rightMax {
				rightMax = arr[right]
			} else {
				trap = trap + rightMax - arr[right]
			}
			right--
		}

	}
	return trap
}
