package main
//
//func main() {
//	head := &ListNode{Val: 1}
//	p := head
//	for i := 2; i < 5; i++ {
//		n := &ListNode{Val: i}
//		p.Next = n
//		p = p.Next
//	}
//	print(head)
//	end := reverseList(head)
//	print(end)
//}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseList(head *ListNode) *ListNode {
	p := head
	var pre *ListNode
	for p != nil {
		next := p.Next
		p.Next = pre
		pre=p
		p = next
	}
	return pre
}
