package main

//func main() {
//	head := &ListNode{Val: 1}
//	p := head
//	arr := []int{2, 1}
//	for i := 0; i < len(arr); i++ {
//		n := &ListNode{Val: arr[i]}
//		p.Next = n
//		p = p.Next
//	}
//	print(head)
//	fmt.Println(isPalindrome(head))
//
//}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func isPalindrome(head *ListNode) bool {
	if head == nil || head.Next == nil {
		return true
	}
	slow := head
	fast := head
	var pre *ListNode
	for fast != nil && fast.Next != nil {
		fast = fast.Next.Next

		next := slow.Next
		slow.Next = pre
		pre = slow
		slow = next

	}
	if fast != nil {
		slow = slow.Next
	}

	for pre != nil && slow != nil {
		if pre.Val != slow.Val {
			return false
		}

		pre = pre.Next
		slow = slow.Next
	}

	return true
}
