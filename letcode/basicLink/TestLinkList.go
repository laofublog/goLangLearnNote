package main

import "testing"

func TestRemoveNthFromEnd(t *testing.T)  {
	head := &ListNode{Val: 4}
	p := head
	for i := 2; i < 10; i++ {
		n := &ListNode{Val: i}
		p.Next = n
		p = p.Next
	}
	end := removeNthFromEnd(head, 2)
	t.Log(end)
}
