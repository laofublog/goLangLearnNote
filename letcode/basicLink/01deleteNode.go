package main

//func main() {
//	head := &ListNode{Val: 4}
//	p := head
//	for i := 2; i < 10; i++ {
//		n := &ListNode{Val: i}
//		p.Next = n
//		p = p.Next
//	}
//	deleteNode(head)
//}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func deleteNode(node *ListNode) {
	node.Val = node.Next.Val
	node.Next = node.Next.Next
}


