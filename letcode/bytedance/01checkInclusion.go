package main

import "fmt"

func main() {
	str1 := "ab"
	str2 := "eidbaooo"
	inclusion := checkInclusion(str1, str2)
	fmt.Println(inclusion)
}

func checkInclusion(str1, str2 string) bool {
	countMap := getCountMap(str1)
	m := copy(countMap)
	mLen, start := 0, 0
	for i := 0; i < len(str2); i++ {
		v, hasKey := m[str2[i]]
		if !hasKey {
			if mLen>0{
				m = copy(countMap)
			}
			start = i + 1
			mLen=0
		} else if v == 0 {
			for start < i && mLen > 0 {
				if str2[start] != str2[i] {
					mLen--
					m[str2[start]]++
					start++
				} else {
					start++
					break
				}
			}
		} else {
			mLen++
			m[str2[i]]--
			if mLen == len(str1) {
				return true
			}

		}
	}
	return false

}
func copy(m map[byte]int) map[byte]int {
	m1 := make(map[byte]int)
	for k, v := range m {
		m1[k] = v
	}
	return m1
}
func getCountMap(str string) map[byte]int {
	count := make(map[byte]int)
	for i := 0; i < len(str); i++ {
		count[str[i]]++
	}
	return count
}
