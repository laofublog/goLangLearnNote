package main

import "fmt"
import "./tree"
import "./treeext"
import "./queue"

func main() {
	var root tree.Node
	root = tree.Node{Value: 3}
	root.Left = &tree.Node{}
	root.Right = &tree.Node{}
	root.Right.Left = new(tree.Node)
	root.Left.Right = tree.CreateNode(2)
	nodes := []tree.Node{
		{Value: 3}, {}, {6, nil, &root},
	}
	root.Left.Right.SetValue(4)
	root.Left.Right.Print()
	//调用方式不发生改变,
	//编译器会根据方法的参数来传递决定传地址还是值。
	root.Left.Right.SetValuePoint(5)
	root.Left.Right.Print()
	fmt.Println(nodes)
	root.Traverse()

	//这样写有问题
	//treeext.NodeEx{Node: &root}.Traverse()
	//
	ex := treeext.NodeEx{Node: &root}
	ex.Traverse()


	//Queue
	q := queue.Queue{1}
	q.Push(2)
	q.Push(3)
	fmt.Println(q.Pop())
	fmt.Println(q.Pop())
	fmt.Println(q.Pop())


}
