package main

import "fmt"

const (
	Cpp    = 0
	java   = 1
	python = 2
)

const (
	b = 1 << (10 * iota)
	kb
	mb
	gb
	tb
	pb
)

func main() {
	fmt.Println(Cpp, java, python)
	fmt.Println(b, kb, mb, gb, tb, pb)
}
