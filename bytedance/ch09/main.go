package main

import "fmt"

func main() {
	a, b := 3, 4
	swap(a, b)

	fmt.Println(a, b)
	swap1(&a, &b)
	fmt.Println(a, b)
}

func swap(a, b int) {
	a, b = b, a
}

func swapreturn(a, b int) (int, int) {
	a, b = b, a
	return a, b
}

func swap1(a, b *int) {
	*a, *b = *b, *a
}
