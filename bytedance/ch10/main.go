package main

import "fmt"

//数组
func main() {
	var arr1 [5]int
	arr2 := [3]int{1, 3, 5}
	//让编译器判断
	arr3 := [...]int{2, 4, 6, 8, 10}

	//多维数组
	var grid [4][5]int
	fmt.Println(arr1, arr2, arr3)
	fmt.Println(grid)

	for i := 0; i < len(arr3); i++ {
		fmt.Println(arr3[i])
	}

	for i, v := range arr3 {
		fmt.Println(i, v)
	}

	for _, v := range arr3 {
		fmt.Println(v)
	}

	printArr(arr1)
	//无法调用 [3]int 和[5]int 是不同的类型
	//printArr(arr2)
	printArr(arr3)
	printArrp(&arr3)

}

//这样是拷贝整个数组，导致内存浪费
func printArr(arr [5]int) {
	fmt.Println("print arr")
	for i, v := range arr {
		fmt.Println(i, v)
	}
}

func printArrp(arr *[5]int) {
	fmt.Println("print arr")
	for i, v := range arr {
		fmt.Println(i, v)
	}
}
