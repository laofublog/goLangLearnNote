package main

import (
	"fmt"
	"time"
)

func main() {
	var arr [11]int
	for i := 0; i < 10; i++ {
		go func(i int) {
			for {
				arr[i]++
				//主动交出控制权
				//runtime.Gosched()
			}
		}(i)
	}
	time.Sleep(time.Minute)
	fmt.Println(arr)
}
