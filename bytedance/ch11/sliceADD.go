package main

import "fmt"

func main() {
	//var arr []int
	arr := make([]int, 2)
	arr[2]=10
	for i := 0; i < 100; i++ {
		arr = append(arr, 2*i+1)
		fmt.Printf("arr=%v,len(arr)=%v cap(arr)=%v\n", arr, len(arr), cap(arr))
	}

	//创建slice
	s2 := make([]int, 16)
	s3 := make([]int, 10, 32) //长度和容量
	fmt.Printf("s2=%v,len(s2)=%v cap(s2)=%v\n", s2, len(s2), cap(s2))
	fmt.Printf("s3=%v,len(s3)=%v cap(s3)=%v\n", s3, len(s3), cap(s3))

	copy(s2, arr)
	fmt.Printf("s2=%v,len(s2)=%v cap(s2)=%v\n", s2, len(s2), cap(s2))
	fmt.Printf("s3=%v,len(s3)=%v cap(s3)=%v\n", s3, len(s3), cap(s3))

	// 删除中间的元素
	s2 = append(s2[:3], s2[4:]...)
	fmt.Printf("s2=%v,len(s2)=%v cap(s2)=%v\n", s2, len(s2), cap(s2))

	front := s2[0]
	s2 = s2[1:]
	tail := s2[len(s2)-1]
	s2 = s2[:len(s2)-1]
	fmt.Printf("front:%v,tail:%v\n", front, tail)
	fmt.Printf("s2=%v,len(s2)=%v cap(s2)=%v\n", s2, len(s2), cap(s2))

}
