package main

import "fmt"

//切片
func main() {
	arr := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println("arr[2:5]", arr[2:5])
	fmt.Println("arr[:5]", arr[:5])
	fmt.Println("arr[2:]", arr[2:])
	fmt.Println("arr[:]", arr[:])
	//fmt.Println("arr[2:5]",arr[2:5])

	s1 := arr[2:6]
	updateArr(s1)
	fmt.Println("s1=", s1)
	fmt.Println(arr)

	//slice再次建立slice,越界
	s2 := s1[3:7]
	fmt.Println("s2=", s2)
	fmt.Printf("s1=%v,len(s1)=%v cap(s1)=%v",s1,len(s1),cap(s1))

	//新的
	s3:= append(s2, 10)
	fmt.Println(s3)
	fmt.Printf("arr=%v,len(arr)=%v cap(arr)=%v",arr,len(arr),cap(arr))
	fmt.Println("\nover cap")
	s4:= append(s3, 10,11,23,12,123)
	fmt.Println(s4)
	fmt.Printf("arr=%v,len(arr)=%v cap(arr)=%v",arr,len(arr),cap(arr))

}

func updateArr(s []int) {
	s[0] = 100
}


