package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

//07.循环
func main() {
	fmt.Println(convertToBin(10),
		convertToBin(15))
	readFile("/Users/fuwei/Documents/笔记/Go语言/01.goLang.md")
}

func convertToBin(n int) string {
	result := ""
	for ; n > 0; n /= 2 {
		lsb := n % 2
		//转数据
		result = strconv.Itoa(lsb) + result

	}
	return result
}

func readFile(filePath string)  {
	open, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	scanner := bufio.NewScanner(open)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}
