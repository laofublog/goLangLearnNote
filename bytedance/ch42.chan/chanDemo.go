package main

import (
	"fmt"
	"time"
)

func main() {
	chandemo1()
	//chanDemo2()
}

func chandemo1() {
	c := make(chan int)
	c <- 1 //这里会阻塞，如果没有发出，就会造成死锁
	c <- 2
	n := <-c
	n = <-c
	fmt.Println(n)

}

func chandemo3() {
	c := make(chan int)
	c <- 1 //这里会阻塞，如果没有发出，就会造成死锁
	n := <-c
	c <- 2
	n = <-c
	fmt.Println(n)

}

func chanDemo2() {
	c := make(chan int)
	go func() {
		for {
			n := <-c
			fmt.Println(n)
		}
	}()
	c <- 1
	c <- 2
	time.Sleep(time.Millisecond)
}