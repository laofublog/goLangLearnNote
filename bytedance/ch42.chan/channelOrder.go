package main

import "fmt"

func main() {
	var c = make(chan int)
	go receive(c)
	c <- 1 //阻塞
	fmt.Println("end")

	cValue := receive1()
	for i:=0;i<100 ;i++  {
		fmt.Println(<-cValue)

	}
}

func receive(c chan int) {
	fmt.Println(<-c)
}

func receive1() chan int {
	c := make(chan int)
	go func() {
		for i := 0; i < 100; i++ {
			c <- i
		}
	}()
	return c
}
