package main

import (
	"fmt"
	"time"
)

func work(id int, c chan int) {
	for i := range c {
		fmt.Printf("work %d,do %c\n", id, i)
	}
}
func createWork(id int) chan<- int {
	c := make(chan int)
	go work(id, c)
	return c
}
func chanDemo() {
	var channels [10]chan<- int

	for i := 0; i < 10; i++ {
		channels[i] = createWork(i)

	}
	for i := 0; i < 10; i++ {
		channels[i] <- 'a' + i
		//n:=<-channels[i]  编译会报错
		//fmt.Println(n)
	}
	for i := 0; i < 10; i++ {
		channels[i] <- 'A' + i
	}
	time.Sleep(time.Millisecond)
}

func bufferedChannel() {
	c := make(chan int, 3)
	go work(0, c)

	c <- 'a'
	c <- 'b'
	c <- 'c'
	c <- 'd'
	time.Sleep(time.Millisecond)
}
func finishSend() {
	c := make(chan int, 3)
	go work(0, c)

	c <- 'a'
	c <- 'b'
	c <- 'c'
	close(c)
	time.Sleep(time.Millisecond)
}
func main() {
	//chanDemo()
	//bufferedChannel()
	finishSend()
}
