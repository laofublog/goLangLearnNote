package main

import (
	"fmt"
	"math"
	"reflect"
	"runtime"
)

func eval(a, b int, op string) (int, error) {
	switch op {
	case "+":
		return a + b, nil
	case "-":
		return a - b, nil
	case "*":
		return a * b, nil
	case "/":
		return a / b, nil
	default:
		return 0, fmt.Errorf("unsupport operation" + op)

	}
}

//多个返回值
func div(a, b int) (q, r int) {
	i, _ := eval(a, b, "/")
	return i, a % b
}

func apply(op func(float64, float64) float64, a, b float64) float64 {
	//通过反射来
	p := reflect.ValueOf(op).Pointer()
	opName := runtime.FuncForPC(p).Name()
	fmt.Println("Calling function is %s with args (%d,%d)", opName, a, b)
	return op(a, b)
}
func sum(nums ...int) int {
	sum := 0
	for i := range nums {
		sum = sum + nums[i]
		fmt.Println(i)
	}
	return sum
}


func main() {
	fmt.Println(eval(3, 4, "*"))
	fmt.Println(eval(12, 4, "/"))
	fmt.Println(div(12, 4))
	//用_代表不用，防止编译报错ßßß
	q, _ := div(12, 4)
	fmt.Println(q)

	//
	fmt.Println(apply(math.Pow, 3.0, 2.0))
	fmt.Println(sum(20, 50))
}
