package retriever

import (
	"net/http"
	"net/http/httputil"
)

type HttpRetriever struct{}

func (retriever *HttpRetriever) Get(url string) string {
	content, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer  content.Body.Close()
	response, err := httputil.DumpResponse(content,true)
	if err!=nil{
		panic(err)
	}
	return string(response)
}
