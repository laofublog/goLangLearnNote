package main

import "fmt"
import "./retriever"

type Retriever interface {
	Get(url string) string
}

func Download(r Retriever) string {
	return r.Get("http://blog.laofu.online")
}

func main() {
	var r Retriever
	r= retriever.MockRetriever{"123123123"}
	c:=r
	fmt.Println(c)
	fmt.Println(Download(r))
	r = &retriever.HttpRetriever{}
	fmt.Println(Download(r))
}
