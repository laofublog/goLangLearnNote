package main

import (
	"bufio"
	"fmt"
	"os"
)

//学习到了32讲，做一个复习和整理
func main() {
	tryDefer()
}

func tryDefer() {
	defer fmt.Println(1)
	defer fmt.Println(2)

	fmt.Println(3)
	//panic("error")
	fmt.Println(4)
	writeFile("./DataTest.txt")
	return
	
}

func writeFile(fileName string)  {
	file,err:=os.Open(fileName)
	if err !=nil{
		if pathError,ok:=err.(*os.PathError);ok{
			fmt.Println(pathError)
		}

		panic(err)
	}
	defer file.Close()

	writer:=bufio.NewWriter(file)
	defer writer.Flush()

	for i := 0; i < 1000; i++ {
		fmt.Fprintln(writer,i)
	}
}