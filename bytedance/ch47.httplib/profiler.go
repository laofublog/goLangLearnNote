package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	//避免没有的时候 报错
	_ "net/http/pprof"
	"os"
)

type appHandler func(writer http.ResponseWriter, request *http.Request) error

func errWrapper(handler appHandler) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		err := handler(writer, request)
		if err != nil {
			switch {
			case os.IsNotExist(err):
				http.Error(writer, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			}
		}
	}
}

func main() {
	http.HandleFunc("/file/", errWrapper(fileList))
	http.HandleFunc("/List/", errWrapper(getDirList))
	err := http.ListenAndServe(":8888", nil)
	if err != nil {

	}
}
func getDirList(writer http.ResponseWriter, request *http.Request) error {
	dir_list, e := ioutil.ReadDir("./")
	if e != nil {
		fmt.Println("read dir error")
		return e
	}

	for _, v := range dir_list {
		bytes := []byte(v.Name() + "\n")
		writer.Write(bytes)
	}
	return e
}
func fileList(writer http.ResponseWriter, request *http.Request) error {

	path := request.URL.Path[len("/List/"):]
	file, err := os.Open(path)
	if err != nil {
		//http.Error(writer, err.Error(), http.StatusInternalServerError)
		return err
	}
	defer file.Close()
	all, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	writer.Write(all)
	return err
}
