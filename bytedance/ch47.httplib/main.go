package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
)

func main() {
	url:="https://www.imooc.com/"
	request, _ := http.NewRequest(http.MethodGet, url, nil)
	request.Header.Add("User-Agent","Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1")
 	response, err := http.DefaultClient.Do(request)
	if err!=nil{
		panic(err)
	}
	client := http.Client{
		CheckRedirect: func(req *http.Request,
			via []*http.Request) error {
			fmt.Println("Redirect,",req)
			return nil

		}}
	client.Do(request)
	defer  response.Body.Close()
	dumpResponse, err := httputil.DumpResponse(response,true)
	if err!=nil{
		panic(err)
	}
	content:=string(dumpResponse)
	fmt.Println(content)
}
