package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var c1, c2 = generator(), generator()
	var worker = createWork(0)
	n := 0
	var values []int
	//产生一个channel 到时间会送入一个值
	after := time.After(10 * time.Second)
	tick := time.Tick(time.Second)
	fmt.Println("开始时间：", time.Now())
	for {
		var activeWorker chan<- int
		var activeValue int
		if len(values) > 0 {
			activeWorker = worker
			activeValue = values[0]
		}

		select {

		//缓存起来排队
		case n = <-c1:
			{
				values = append(values, n)
			}
		case n = <-c2:
			{
				values = append(values, n)
			}
			//负责打印
		case activeWorker <- activeValue:
			{
				values = values[1:]
			}
		case <-time.After(800 * time.Millisecond):
			{
				fmt.Println("timeout")
			}
		case <-tick:
			{
				fmt.Println("queue len:", len(values))
			}
			//到时间退出
		case <-after:
			{
				fmt.Println("到时间结束：", time.Now())
				return
			}
			//非阻塞
		default:
			{

			}
		}

	}
}
func work(id int, c chan int) {

	for i := range c {
		time.Sleep(time.Second)
		fmt.Printf("work %d,do %d\n", id, i)
	}
}
func createWork(id int) chan<- int {
	c := make(chan int)
	go work(id, c)
	return c
}
func generator() chan int {
	out := make(chan int)
	go func() {
		i := 0
		for {
			fmt.Println("生成数字", i)
			time.Sleep(time.Duration(rand.Intn(1500)) * time.Millisecond)
			out <- i
			i++
		}
	}()
	return out
}
