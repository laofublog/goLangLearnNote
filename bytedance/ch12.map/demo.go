package main

import "fmt"

func main() {
	str := "abcdacbdacbd"
	fmt.Println(norepeateSubstr(str))

	strName := "我是中国人！"

	for i, ch := range strName {
		fmt.Printf("  index:%v str:%c", i, ch)
	}
	for i, ch := range []rune(strName) {
		fmt.Printf("  index:%v str:%c", i, ch)
	}
}

func norepeateSubstr(s string) int {
	start := 0
	m := make(map[string]int)
	max := 0
	for i, ch := range []byte(s) {
		if v, hasKey := m[string(ch)]; hasKey && v >= start {
			start = m[string(ch)] + 1
		}
		if i-start+1 > max {
			max = i - start + 1
		}
		m[string(ch)] = i
	}
	return max
}
