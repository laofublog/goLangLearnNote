package main

import "fmt"

func main(){
	m:=map[string]string{
		"key1":"val1",
		"key2":"val2",
		"key3":"val3",
		"key4":"val4",
		"key5":"val5",
	}
	m2:=make(map[string]int) //m2==empty
	var m3 map[string] int  //nil 可以参与安全运算
	fmt.Println(m,m2,m3)
	//map是无序的，每次顺序都不同
	for k,v :=range m{
		fmt.Printf("\n Key:%s,Value:%s\n",k,v)
	}

	fmt.Println(m["key1"])
	fmt.Println(m["key10"])

	//判断是否存在
	if s,hasKey := m["key10"];hasKey{
		fmt.Println(s)
	}else {
		fmt.Println("key not exist")
	}

    delete(m,"key1")
	fmt.Println(m)

}
