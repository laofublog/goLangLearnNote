package main

import (
	"fmt"
	"math"
)

func main() {
	a, b := 3, 4
	c := int(math.Sqrt(float64(a*a + b*b)))
	fmt.Print(c)

	const e, f = 3, 4
	g := math.Sqrt(float64(e*e + f*f))
	fmt.Print(g)
}
