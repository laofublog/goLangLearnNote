package main

import "fmt"

type Node struct {
	Value       int
	Left, Right *Node
}

//类似构造函数
//此处返回的是局部变量的地址
func CreateNode(value int) *Node {
	return &Node{Value: value}
}

//代表方法成员方法
func (node Node) Print() {
	fmt.Println(node.Value)
}

//这个方法是传值的，无法修改对象的值
func (node Node) SetValue(value int) {
	node.Value = value
}
func (node *Node) SetValuePoint(value int) {
	node.Value = value
}

//使用是相同，但是是传值的
func print(node Node) {
	fmt.Println(node.Value)
}

func (node *Node) Traverse() {
	if node == nil {
		return
	}
	node.Left.Traverse()
	node.Print()
	node.Right.Traverse()
}

func main() {

	treeRoot := Node{Value: 1}
	treeRoot.Left = &Node{Value: 2}
	treeRoot.Right = &Node{Value: 3}
	treeRoot.Left.Left = &Node{Value: 4}
	treeRoot.Left.Right = &Node{Value: 5}
	treeRoot.Right.Left = &Node{Value: 6}
	treeRoot.Right.Right = &Node{Value: 7}
	//       1
	//     /   \
	//    2     3
	//   / \   / \
	//  4  5  6   7
	//
	treeRoot.Traverse()
}


