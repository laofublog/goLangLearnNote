package main

import (
	"fmt"
	"sync"
)

type worker struct {
	in chan int
	wg *sync.WaitGroup
}

func work(id int, c chan int,wg *sync.WaitGroup) {
	for i := range c {
		fmt.Printf("work %d,do %c\n", id, i)
		wg.Done()
	}
}
func createWork(id int,wg *sync.WaitGroup) worker {
	w := worker{
		in: make(chan int),
	}
	go work(id, w.in,wg)
	return w
}
func chanDemo() {
	var wg sync.WaitGroup
	var workers [10]worker
	for i := 0; i < 10; i++ {
		workers[i] = createWork(i,&wg)
	}
	for i, worker := range workers {
		worker.in <- 'a' + i
		wg.Add(1)
	}

	for i, worker := range workers {
		worker.in <- 'A' + i
		wg.Add(1)
	}

	wg.Wait()
	//time.Sleep(time.Millisecond)
}

func main() {
	chanDemo()
	//bufferedChannel()

}
