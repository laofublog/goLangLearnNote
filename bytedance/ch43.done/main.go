package main

import (
	"fmt"
)

type worker struct {
	in   chan int
	done chan bool
}

func work(id int, c chan int, done chan bool) {
	for i := range c {
		fmt.Printf("work %d,do %c\n", id, i)
		//go func() { done <- true }()
		done <- true
	}
}
func createWork(id int) worker {
	w := worker{
		in:   make(chan int),
		done: make(chan bool),
	}
	go work(id, w.in, w.done)
	return w
}
func chanDemo() {
	var workers [10]worker
	for i := 0; i < 10; i++ {
		workers[i] = createWork(i)
	}
	for i, worker := range workers {
		worker.in <- 'a' + i
	}
	for _, worker := range workers {
		<-worker.done

	}
	for i, worker := range workers {
		worker.in <- 'A' + i
	}
	for _, worker := range workers {
		<-worker.done

	}
	//time.Sleep(time.Millisecond)
}

func main() {
	chanDemo()
	//bufferedChannel()

}
