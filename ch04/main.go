package main

import "fmt"

func main() {
	array := [5]int{10, 20, 30, 40, 50}
	array[2] = 10

	//使用指针
	arrayPoint := [5]*int{0: new(int), 1: new(int)}
	fmt.Print(* arrayPoint[0])
	fmt.Print(*  arrayPoint[1])
	fmt.Print(*  arrayPoint[2])
	* arrayPoint[1] = 20
}
