package treeext

import "../tree"

type NodeEx struct {
	Node *tree.Node
}

func (nodeEx *NodeEx) Traverse() {
	if nodeEx == nil || nodeEx.Node == nil {
		return
	}

	nodeEx.Node.Right.Traverse()
	nodeEx.Node.Print()
	nodeEx.Node.Left.Traverse()
}
